#![no_std]
#[cfg(test)]
extern crate alloc;

#[derive(Copy, Clone)]
pub struct Mbi5153Config {
    pub lower_ghost_elimination: bool,
    pub scan_line_count: u8,
    pub gray_scale_mode: bool,
    pub gclk_multiplier: bool,
    pub current_gain: u8,
}

impl Default for Mbi5153Config {
    fn default() -> Self {
        Mbi5153Config {
            lower_ghost_elimination: false,
            scan_line_count: 4,
            gray_scale_mode: false,
            gclk_multiplier: false,
            current_gain: 0b101011,
        }
    }
}

#[derive(Copy, Clone)]
#[repr(u8)]
pub enum DimLineCompensation {
    Zero = 0b000,
    Five = 0b001,
    Ten = 0b010,
    Fifteen = 0b011,
    Twenty = 0b100,
    TwentyFive = 0b101,
    Thirty = 0b110,
    ThirtyFive = 0b111,
}

#[derive(Copy, Clone)]
pub struct Mbi5153Config2 {
    pub mystery_fedcb: u8,
    pub double_refresh_rate: bool,
    pub mystery_987654: u8,
    pub dim_line_compensation: DimLineCompensation,
    pub mystery_0: bool,
}

impl Mbi5153Config2 {
    pub fn novastar_secret_sauce_r() -> Self {
        Self {
            mystery_fedcb: 0b01000,
            double_refresh_rate: false,
            mystery_987654: 0,
            dim_line_compensation: DimLineCompensation::Zero,
            mystery_0: false,
        }
    }

    pub fn novastar_secret_sauce_gb() -> Self {
        Self {
            mystery_fedcb: 0b01100,
            double_refresh_rate: false,
            mystery_987654: 0b000001,
            dim_line_compensation: DimLineCompensation::Zero,
            mystery_0: false,
        }
    }

    pub fn appnote_secret_sauce_r() -> Self {
        Self {
            mystery_fedcb: 0b01000,
            double_refresh_rate: false,
            mystery_987654: 0b100000,
            dim_line_compensation: DimLineCompensation::Zero,
            mystery_0: true,
        }
    }

    pub fn appnote_secret_sauce_gb() -> Self {
        Self {
            mystery_fedcb: 0b01100,
            double_refresh_rate: false,
            mystery_987654: 0b100000,
            dim_line_compensation: DimLineCompensation::Zero,
            mystery_0: false,
        }
    }
}

impl Default for Mbi5153Config2 {
    fn default() -> Self {
        Mbi5153Config2 {
            mystery_fedcb: 0,
            double_refresh_rate: false,
            mystery_987654: 0,
            dim_line_compensation: DimLineCompensation::Zero,
            mystery_0: false,
        }
    }
}

impl From<Mbi5153Config> for [bool; 16] {
    fn from(value: Mbi5153Config) -> Self {
        let mut ret = [false; 16];
        ret[0] = value.lower_ghost_elimination;
        ret[1] = false; // reserved
        ret[2] = false; // reserved

        // scanned line
        let cnt = value.scan_line_count.saturating_sub(1);
        ret[3] = (cnt & 0b10000) > 0;
        ret[4] = (cnt & 0b01000) > 0;
        ret[5] = (cnt & 0b00100) > 0;
        ret[6] = (cnt & 0b00010) > 0;
        ret[7] = (cnt & 0b00001) > 0;

        ret[8] = value.gray_scale_mode;
        ret[9] = value.gclk_multiplier;

        // Current gain:
        let gain = value.current_gain;
        ret[10] = (gain & 0b100000) > 0;
        ret[11] = (gain & 0b010000) > 0;
        ret[12] = (gain & 0b001000) > 0;
        ret[13] = (gain & 0b000100) > 0;
        ret[14] = (gain & 0b000010) > 0;
        ret[15] = (gain & 0b000001) > 0;
        ret
    }
}

impl From<Mbi5153Config2> for [bool; 16] {
    fn from(value: Mbi5153Config2) -> Self {
        let mut ret = [false; 16];
        ret[0] = (value.mystery_fedcb & 0b10000) > 0; // reserved
        ret[1] = (value.mystery_fedcb & 0b01000) > 0; // reserved
        ret[2] = (value.mystery_fedcb & 0b00100) > 0; // reserved
        ret[3] = (value.mystery_fedcb & 0b00010) > 0; // reserved
        ret[4] = (value.mystery_fedcb & 0b00001) > 0; // reserved

        ret[5] = value.double_refresh_rate;

        ret[6] = (value.mystery_987654 & 0b100000) > 0; // reserved
        ret[7] = (value.mystery_987654 & 0b010000) > 0; // reserved
        ret[8] = (value.mystery_987654 & 0b001000) > 0; // reserved
        ret[9] = (value.mystery_987654 & 0b000100) > 0; // reserved
        ret[10] = (value.mystery_987654 & 0b000010) > 0; // reserved
        ret[11] = (value.mystery_987654 & 0b000001) > 0; // reserved

        let dlc = value.dim_line_compensation as u8;
        ret[12] = (dlc & 0b100) > 0;
        ret[13] = (dlc & 0b010) > 0;
        ret[14] = (dlc & 0b001) > 0;

        ret[15] = value.mystery_0; // reserved

        ret
    }
}

pub struct ConfigSender {
    value: [bool; 16],
    ic_count: usize,
    index: usize,
    raised: bool,
    rising_edges: usize,
}

impl ConfigSender {
    pub fn new(cfg: Mbi5153Config, ic_count: u8) -> Self {
        Self {
            value: cfg.into(),
            ic_count: ic_count as usize,
            index: 0,
            raised: false,
            rising_edges: 4,
        }
    }

    pub fn new_2(cfg: Mbi5153Config2, ic_count: u8) -> Self {
        Self {
            value: cfg.into(),
            ic_count: ic_count as usize,
            index: 0,
            raised: false,
            rising_edges: 8,
        }
    }
}

impl Iterator for ConfigSender {
    type Item = PanelOutputs<1>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= (16 * self.ic_count) {
            return None;
        }
        let current_ic = (self.index / 16) + 1;
        let value_index = self.index % 16;
        let value = self.value[value_index];
        let le = current_ic == self.ic_count && value_index >= (16 - self.rising_edges);
        if self.raised {
            self.index += 1;
            self.raised = false;
            Some(PanelOutputs {
                dclk: true,
                le,
                sdis: [value],
            })
        } else {
            self.raised = true;
            Some(PanelOutputs {
                dclk: false,
                le,
                sdis: [value],
            })
        }
    }
}

#[derive(Default, Copy, Clone, PartialEq)]
pub struct AddressOutputs {
    pub addr_a: bool,
    pub addr_b: bool,
    pub addr_c: bool,
    pub addr_d: bool,
    pub addr_e: bool,
    pub addrs_changed: bool,
    pub gclk: bool,
}

pub struct AddressState {
    /// The number of scanlines to drive.
    num_scanlines: u8,
    /// The number of cycles to wait during dead time.
    dead_time_count: usize,
    /// The number of cycles left in the current dead time.
    dead_time_left: usize,
    /// The number of GCLKs per scanline. Note that the last one is treated
    /// specially, as a "dead time".
    ///
    /// (Almost always 1024, but it makes writing tests easier
    /// if you can make this smaller)
    gclks_per_scanline: u16,
    /// The number of GCLKs left on the current scanline.
    gclks_left: u16,
    /// The current scanline (zero-indexed).
    scanline: u8,
    /// The last set of outputs (stored here to avoid recalculating the addresses
    /// every time).
    outputs: AddressOutputs,
}

impl AddressState {
    pub fn new(num_scanlines: u8, dead_time_count: usize, gclks_per_scanline: u16) -> Self {
        Self {
            num_scanlines,
            dead_time_count,
            dead_time_left: 0,
            gclks_per_scanline,
            gclks_left: gclks_per_scanline,
            scanline: 0,
            outputs: AddressOutputs {
                addr_a: false,
                addr_b: false,
                addr_c: false,
                addr_d: false,
                addr_e: false,
                gclk: true,
                addrs_changed: true,
            },
        }
    }

    pub fn reset(&mut self) {
        self.dead_time_left = 0;
        self.gclks_left = self.gclks_per_scanline;
        self.scanline = 0;
        self.outputs = AddressOutputs {
            addr_a: false,
            addr_b: false,
            addr_c: false,
            addr_d: false,
            addr_e: false,
            gclk: true,
            addrs_changed: true,
        };
    }
}

impl Iterator for AddressState {
    type Item = AddressOutputs;

    fn next(&mut self) -> Option<Self::Item> {
        if self.outputs.addrs_changed {
            self.outputs.addrs_changed = false;
        }
        if self.outputs.gclk {
            self.outputs.gclk = false;

            if self.gclks_left == 0 {
                self.dead_time_left = self.dead_time_count;
            }
        } else if self.gclks_left == 0 && self.dead_time_left > 0 {
            if self.dead_time_left == self.dead_time_count {
                self.outputs.addr_a = false;
                self.outputs.addr_b = false;
                self.outputs.addr_c = false;
                self.outputs.addr_d = false;
                self.outputs.addr_e = false;
                self.outputs.addrs_changed = true;
            }
            self.dead_time_left -= 1;
        } else {
            if self.gclks_left == 0 {
                // Out of dead time? Time to advance to the next scanline,
                // and reset all the counters.
                self.scanline = (self.scanline + 1) % self.num_scanlines;
                self.gclks_left = self.gclks_per_scanline;

                self.outputs.addr_a = (self.scanline & 0b00001) > 0;
                self.outputs.addr_b = (self.scanline & 0b00010) > 0;
                self.outputs.addr_c = (self.scanline & 0b00100) > 0;
                self.outputs.addr_d = (self.scanline & 0b01000) > 0;
                self.outputs.addr_e = (self.scanline & 0b10000) > 0;
                self.outputs.addrs_changed = true;
            }

            self.outputs.gclk = true;
            self.gclks_left -= 1;
        }
        Some(self.outputs)
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct PanelOutputs<const N: usize> {
    pub dclk: bool,
    pub le: bool,
    pub sdis: [bool; N],
}

pub struct PanelState<'a, const N: usize> {
    /// Interleaved grayscale data to be output.
    data: [&'a [u16]; N],
    /// The current u16 word we are outputting.
    current: [u16; N],
    /// Which bit of the u16 word we are outputting.
    current_index: u8,
    /// Whether or not DCLK should be raised on the next cycle.
    raised: bool,
    /// The number of chained ICs (used to figure out when to assert LE).
    ic_count: u8,
    /// The current IC we are outputting the set of data for (zero-indexed)
    ic_index: u8,
    /// A mask to apply to each u16 word of the data.
    mask: u16,
}

impl<'a, const N: usize> PanelState<'a, N> {
    pub fn new(data: [&'a [u16]; N], ic_count: u8, mask: u16) -> Self {
        assert_eq!(data[0].len() % ic_count as usize, 0);
        Self {
            data,
            current: [0; N],
            current_index: 16,
            raised: false,
            ic_count,
            ic_index: ic_count - 1,
            mask,
        }
    }

    // Get the `bit`-th bit of `self.current`, counting with the MSB being 0
    // and the LSB being 15.
    fn current_bit(&self, n: usize, bit: u8) -> bool {
        ((self.current[n] >> (15 - bit)) & 1) > 0
    }

    fn current_bits(&self, bit: u8) -> [bool; N] {
        let mut ret = [false; N];
        for (n, out) in ret.iter_mut().enumerate() {
            *out = self.current_bit(n, bit);
        }
        ret
    }
}

// What we want is (example 4-bit data)
//
// L = low, H = high, 0/1/2 = 0/1/2nd bit of data
//
// cycle   0  1  2  3  4  5  6  7
// -----+------------------------ etc
// DCLK |  L  H  L  H  L  H  L  H  L
//  SDI |  0  0  1  1  2  2  3  3  0
//   LE |  L  L  L  L  L  L  H  H  L
//
// i.e.
// - keep DCLK low, set SDI to next bit
// - raise DCLK high, keep SDI as is
// - set DCLK low, set SDI to next bit
// - ..etc
// (and then, on the last bit, assert LE)

impl<'a, const N: usize> Iterator for PanelState<'a, N> {
    type Item = PanelOutputs<N>;

    fn next(&mut self) -> Option<Self::Item> {
        // If our index is 16, we've exhausted `self.current` (or we're just at the start),
        // so we need a new u16 word to clock out.
        if self.current_index == 16 {
            for (i, data) in self.data.iter_mut().enumerate() {
                let (current, rest) = data.split_first()?;
                self.current[i] = current & self.mask;
                *data = rest;
            }
            self.current_index = 0;
            // We want the next cycle to assert DCLK and advance the bit.
            self.raised = true;
            self.ic_index = (self.ic_index + 1) % self.ic_count;
            Some(PanelOutputs {
                dclk: false,
                le: false,
                sdis: self.current_bits(0),
            })
        } else {
            let sdis = self.current_bits(self.current_index);
            let le = self.current_index == 15 && (self.ic_index + 1) == self.ic_count;
            let dclk = if self.raised {
                self.current_index += 1;
                self.raised = false;
                true
            } else {
                self.raised = true;
                false
            };
            Some(PanelOutputs { dclk, le, sdis })
        }
    }
}

pub fn address_counter<const BITS: usize>(scanlines: usize) -> impl Iterator<Item = [bool; BITS]> {
    (0..scanlines).map(|scanline| {
        let mut ret = [false; BITS];
        for (bit, out) in ret.iter_mut().rev().enumerate() {
            *out = (scanline & (1 << bit)) > 0;
        }
        ret
    })
}

#[cfg(test)]
mod test {
    use crate::{
        address_counter, AddressState, ConfigSender, Mbi5153Config, PanelOutputs, PanelState,
    };

    use alloc::string::{String, ToString};
    use alloc::vec;
    use alloc::vec::Vec;

    fn filter_underscores(inp: &str) -> String {
        inp.chars().filter(|&x| x != '_').collect()
    }

    fn check_outputs<const N: usize>(
        output: &[PanelOutputs<N>],
        sdis: [&str; N],
        dclk: &str,
        le: &str,
    ) {
        let sdi_os = (0..N)
            .map(|n| {
                output
                    .iter()
                    .map(|x| (x.sdis[n] as u8).to_string())
                    .collect::<String>()
            })
            .collect::<Vec<_>>();
        let dclk_o = output
            .iter()
            .map(|x| (x.dclk as u8).to_string())
            .collect::<String>();
        let le_o = output
            .iter()
            .map(|x| (x.le as u8).to_string())
            .collect::<String>();
        for (n, sdi_o) in sdi_os.into_iter().enumerate() {
            assert_eq!(sdi_o, filter_underscores(sdis[n]));
        }
        assert_eq!(dclk_o, filter_underscores(dclk));
        assert_eq!(le_o, filter_underscores(le));
    }

    #[test]
    fn test_address_counter() {
        assert_eq!(
            address_counter::<2>(4).collect::<Vec<_>>(),
            vec![[false, false], [false, true], [true, false], [true, true]]
        );
    }

    #[test]
    fn most_basic_2_channels() {
        let data = vec![42, 7];
        let state = PanelState::new([&data], 1, u16::MAX);
        let output = state.collect::<Vec<_>>();

        // 42 = 0b0000_0000_0010_1010
        check_outputs(
            &output[0..32],
            /* SDI  */ ["00000000_00000000_00001100_11001100"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000011",
        );

        // 7  = 0b0000_0000_0000_0111
        check_outputs(
            &output[32..],
            /* SDI  */ ["00000000_00000000_00000000_00111111"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000011",
        );
    }

    #[test]
    fn two_by_two_channels() {
        let data_a = vec![42, 7];
        let data_b = vec![7, 42];
        let state = PanelState::new([&data_a, &data_b], 1, u16::MAX);
        let output = state.collect::<Vec<_>>();

        // 42 = 0b0000_0000_0010_1010
        check_outputs(
            &output[0..32],
            /* SDI  */
            [
                "00000000_00000000_00001100_11001100",
                "00000000_00000000_00000000_00111111",
            ],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000011",
        );

        // 7  = 0b0000_0000_0000_0111
        check_outputs(
            &output[32..],
            /* SDI  */
            [
                "00000000_00000000_00000000_00111111",
                "00000000_00000000_00001100_11001100",
            ],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000011",
        );
    }

    #[test]
    fn chained_two_channels() {
        let data = vec![42, 65535, 7, 128];
        let state = PanelState::new([&data], 2, u16::MAX);
        let output = state.collect::<Vec<_>>();

        // 42 = 0b0000_0000_0010_1010
        check_outputs(
            &output[0..32],
            /* SDI  */ ["00000000_00000000_00001100_11001100"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000000",
        );

        // 65535  = 0b1111_1111_1111_1111
        check_outputs(
            &output[32..64],
            /* SDI  */ ["11111111_11111111_11111111_11111111"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000011",
        );
    }

    #[test]
    fn basic_addressing() {
        let addr = AddressState::new(3, 5, 4);
        let output = addr.take(8 * 4 + 5 * 3).collect::<Vec<_>>();

        let gclks = output
            .iter()
            .map(|x| (x.gclk as u8).to_string())
            .collect::<String>();

        assert_eq!(
            gclks,
            //                      | line 0 | dead| line 1 | dead| line 2 | dead| line 0
            filter_underscores("01010101_00000_01010101_00000_01010101_00000_01010101")
        );

        let addr_a = output
            .iter()
            .map(|x| (x.addr_a as u8).to_string())
            .collect::<String>();

        assert_eq!(
            addr_a,
            //                      | line 0 | dead| line 1 | dead| line 2 | dead| line 0
            filter_underscores("00000000_00000_01111111_10000_00000000_00000_00000000")
        );

        let addr_b = output
            .iter()
            .map(|x| (x.addr_b as u8).to_string())
            .collect::<String>();

        assert_eq!(
            addr_b,
            //                      | line 0 | dead| line 1 | dead| line 2 | dead| line 0
            filter_underscores("00000000_00000_00000000_00000_01111111_10000_00000000")
        );
    }

    #[test]
    fn default_config() {
        let cfg = Mbi5153Config::default();
        let ser: [bool; 16] = cfg.into();
        let ser = ser
            .iter()
            .map(|x| if *x { '1' } else { '0' })
            .collect::<String>();
        assert_eq!(ser, "0000001100101011");
    }

    #[test]
    fn config_sender_1x() {
        let cfg = Mbi5153Config::default();
        let sender = ConfigSender::new(cfg, 1);
        let output = sender.collect::<Vec<_>>();

        // intended output as above: "0000 0011 0010 1011"
        check_outputs(
            &output,
            /* SDI  */ ["00000000_00001111_00001100_11001111"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_11111111",
        );
    }

    #[test]
    fn config_sender_2x() {
        let cfg = Mbi5153Config::default();
        let sender = ConfigSender::new(cfg, 2);
        let output = sender.collect::<Vec<_>>();

        // intended output as above: "0000 0011 0010 1011"
        // but with LE not asserted (first IC)
        check_outputs(
            &output[0..32],
            /* SDI  */ ["00000000_00001111_00001100_11001111"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_00000000",
        );

        // intended output as above: "0000 0011 0010 1011"
        // but with LE asserted
        check_outputs(
            &output[32..],
            /* SDI  */ ["00000000_00001111_00001100_11001111"],
            /* DCLK  */ "01010101_01010101_01010101_01010101",
            /* LE    */ "00000000_00000000_00000000_11111111",
        );
    }
}
