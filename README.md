# led-panel-zone

This repository contains code for interfacing with
[DMG1083 LED tiles](https://led.limehouselabs.org/docs/tiles/dmg1083/), using a custom-built PCB based on the
[RP2040](https://www.raspberrypi.com/documentation/microcontrollers/rp2040.html)
microcontroller from Raspberry Pi.

## Hardware

There are two PCB revisions: rev1, based around a Pi Pico devboard and solderable components, and rev2, a
surface-mount design intended to be assembled by [JLCPCB](https://jlcpcb.com/).
The [KiCad](https://www.kicad.org/) project files for each revision are in `pcbs/`.

Most development effort has been focused around rev2 boards lately. If you're ordering a new board,
you should aim for a rev2 one.

(ideally, a future revision of the code would let you compile all the crates for either revision, but
that's not been done yet)

### Interested in a plug-and-play solution?

As of 2024-06-04, I'm thinking of doing a bulk order of the rev2 boards. If you'd be interested,
[get in touch](http://eta.st/#contact).

## Contents

The repository consists of multiple Rust crates in a Cargo workspace:

- `mbi5153` contains software iterators for generating signals for the MBI5153 driver chips on the panel, as well as
  tests.
- `dmg1083` contains some constants about the panel geometry, and some code for preparing suitable framebuffers with
  `embedded-graphics`.
- `kittywave-software` uses the above crates to drive the panel entirely in software, displaying a static cute cat
  waving emoji.
  - NOTE: this crate currently assumes a rev1 board.
- `dmg1083-pio` contains RP2040 Programmable I/O assembly programs for driving the panel with hardware acceleration, and
  a library-like interface for sending frames with these programs.
- `squishomatic` uses the `dmg1083-pio` crate to display an animated image (taking advantage of the increased speed).
  - NOTE: this crate currently assumes a rev1 board.
- **`usb2panel`** makes the board behave as a USB device that can have images (in [QOI](http://qoiformat.org/)
  format) streamed to it by `usb2panel-host`. This is probably what you want to flash onto your board.
  - NOTE: this crate currently assumes a rev2 board.
- `usb2panel-host` implements the host side of the USB protocol.

Additionally:

- `webshit` contains some WebUSB-based pages that can speak the USB protocol, including some "video wall"-type
  functionality.

## Getting started with development

You'll need [Rust](https://www.rust-lang.org) installed, and the toolchain for the Pi Pico as described
[here](https://github.com/rp-rs/rp-hal/tree/main#getting-started).

Make sure you have a [Picoprobe](https://github.com/raspberrypi/picoprobe) connected and plugged in to the board to
develop on (you can use `probe-rs debug` to verify that works).

If you're going to upload via the RP2040 USB bootloader instead, edit `.cargo/config` as indicated by the comments
therein.

Then, just run

```
$ cargo run --release -p usb2panel
# or (if you have a rev1 board)
$ cargo run --release -p squishomatic
```


