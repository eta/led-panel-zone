#![no_std]
#![no_main]

use core::cell::RefCell;
use core::panic::PanicInfo;
use dmg1083::{PanelDataPio, PIO_DATA_LEN, SCAN_LINES};
use dmg1083_pio::Dmg1083;
use embedded_graphics::image::Image;
use embedded_graphics_core::prelude::*;
use embedded_hal::digital::v2::OutputPin;
use mbi5153::Mbi5153Config;
use misc_hacks::{InvertedPin, PinGroup, PinRef, Syncify};
use rp_pico::hal::dma::DMAExt;
use rp_pico::{entry, hal, pac, Pins};
use rtt_target::{rprintln, rtt_init_print};
use tinybmp::Bmp;

mod frameflip;
mod data;

#[panic_handler]
fn panic_handler(i: &PanicInfo) -> ! {
    rprintln!("{}", i);
    loop {}
}

// SAFETY: We only access `DATA` from one core.
static DATA: Syncify<RefCell<[u32; PIO_DATA_LEN]>> =
    unsafe { Syncify::new(RefCell::new([!0; PIO_DATA_LEN])) };

#[entry]
fn main() -> ! {
    rtt_init_print!();

    rprintln!("hi from the squishomatic o/");

    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let _core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let _clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // The single-cycle I/O block controls our GPIO pins
    let sio = hal::Sio::new(pac.SIO);

    // Set the pins up according to their function on this particular board
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Address lines
    let addr_a = pins.gpio2.into_push_pull_output().into_dyn_pin();
    let addr_b = pins.gpio3.into_push_pull_output().into_dyn_pin();
    let addr_c = pins.gpio4.into_push_pull_output().into_dyn_pin();
    let addr_d = pins.gpio5.into_push_pull_output().into_dyn_pin();
    let addr_e = pins.gpio6.into_push_pull_output().into_dyn_pin();

    // Clocks
    let gclk = pins.gpio7.into_push_pull_output().into_dyn_pin();
    let mut dclk = pins.gpio8.into_push_pull_output().into_dyn_pin();
    let mut latch = pins.gpio9.into_push_pull_output().into_dyn_pin();

    // Data lines
    let mut r1_sdi = pins.gpio10.into_push_pull_output().into_dyn_pin();
    let mut g1_sdi = pins.gpio11.into_push_pull_output().into_dyn_pin();
    let mut b1_sdi = pins.gpio12.into_push_pull_output().into_dyn_pin();
    let mut r2_sdi = pins.gpio13.into_push_pull_output().into_dyn_pin();
    let mut g2_sdi = pins.gpio14.into_push_pull_output().into_dyn_pin();
    let mut b2_sdi = pins.gpio15.into_push_pull_output().into_dyn_pin();
    // note schematic fuckery:
    // G3 actually controls R3
    // (R3 actually controls G3)
    // B4 actually controls B3
    // (B3 actually controls B4)
    // G4 actually controls R4
    // (R4 actually controls G4)
    let mut r3_sdi = pins.gpio17.into_push_pull_output().into_dyn_pin();
    let mut g3_sdi = pins.gpio16.into_push_pull_output().into_dyn_pin();
    let mut b3_sdi = pins.gpio21.into_push_pull_output().into_dyn_pin();
    let mut r4_sdi = pins.gpio20.into_push_pull_output().into_dyn_pin();
    let mut g4_sdi = pins.gpio19.into_push_pull_output().into_dyn_pin();
    let mut b4_sdi = pins.gpio18.into_push_pull_output().into_dyn_pin();
    // (end schematic fuckery)

    // SR pin that needs to be low to work
    let mut sr = pins.gpio22.into_push_pull_output().into_dyn_pin();
    sr.set_high().unwrap();

    rprintln!("configuring panel...");

    let sdis = PinGroup([
        InvertedPin(PinRef(&mut r1_sdi)),
        InvertedPin(PinRef(&mut r2_sdi)),
        InvertedPin(PinRef(&mut r3_sdi)),
        InvertedPin(PinRef(&mut r4_sdi)),
        InvertedPin(PinRef(&mut g1_sdi)),
        InvertedPin(PinRef(&mut g2_sdi)),
        InvertedPin(PinRef(&mut g3_sdi)),
        InvertedPin(PinRef(&mut g4_sdi)),
        InvertedPin(PinRef(&mut b1_sdi)),
        InvertedPin(PinRef(&mut b2_sdi)),
        InvertedPin(PinRef(&mut b3_sdi)),
        InvertedPin(PinRef(&mut b4_sdi)),
    ]);

    let config = Mbi5153Config {
        scan_line_count: SCAN_LINES as _,
        current_gain: 255,
        gclk_multiplier: true,
        ..Default::default()
    };

    dmg1083::configure_panel(
        config,
        InvertedPin(PinRef(&mut dclk)),
        InvertedPin(PinRef(&mut latch)),
        sdis,
    )
    .unwrap();

    rprintln!("configuring PIO...");

    // FIXME(eta): You know, I'm not sure the typestates are actually helping.

    let mut panel = Dmg1083::new_pio0(
        pac.PIO0,
        &mut pac.RESETS,
        true, // should_invert
        config.gclk_multiplier,
        dclk.try_into_function().ok().unwrap(),
        gclk.try_into_function().ok().unwrap(),
        latch.try_into_function().ok().unwrap(),
        [
            r1_sdi.try_into_function().ok().unwrap(),
            g1_sdi.try_into_function().ok().unwrap(),
            b1_sdi.try_into_function().ok().unwrap(),
            r2_sdi.try_into_function().ok().unwrap(),
            g2_sdi.try_into_function().ok().unwrap(),
            b2_sdi.try_into_function().ok().unwrap(),
            g3_sdi.try_into_function().ok().unwrap(),
            r3_sdi.try_into_function().ok().unwrap(),
            b4_sdi.try_into_function().ok().unwrap(),
            g4_sdi.try_into_function().ok().unwrap(),
            r4_sdi.try_into_function().ok().unwrap(),
            b3_sdi.try_into_function().ok().unwrap(),
        ],
        [
            addr_a.try_into_function().ok().unwrap(),
            addr_b.try_into_function().ok().unwrap(),
            addr_c.try_into_function().ok().unwrap(),
            addr_d.try_into_function().ok().unwrap(),
            addr_e.try_into_function().ok().unwrap(),
        ],
    );

    rprintln!("sending squishes :3");

    let dma = pac.DMA.split(&mut pac.RESETS);
    let mut ch0 = Some(dma.ch0);

    loop {
        for squish in data::SQUISHES {
            {
                let mut data = DATA.0.borrow_mut();
                *data = [!0; PIO_DATA_LEN];
                let mut wrapper = PanelDataPio(&mut data);
                let bmp = Bmp::from_slice(squish).unwrap();
                Image::new(&bmp, Point::new(2, 0))
                    .draw(&mut wrapper)
                    .unwrap();
            }

            ch0 = Some(panel.transmit_frame(DATA.0.borrow(), ch0.take().unwrap()));
            panel.vsync();
        }
    }
}
