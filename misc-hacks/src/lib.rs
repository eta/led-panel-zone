#![no_std]

use embedded_hal::digital::v2::{OutputPin, PinState};

/// Paper over the lack of `impl<'a> OutputPin for &'a mut impl OutputPin`.
pub struct PinRef<'a, I>(pub &'a mut I);

impl<'a, I> OutputPin for PinRef<'a, I>
where
    I: OutputPin,
{
    type Error = I::Error;

    #[inline(always)]
    fn set_low(&mut self) -> Result<(), Self::Error> {
        self.0.set_low()
    }

    #[inline(always)]
    fn set_high(&mut self) -> Result<(), Self::Error> {
        self.0.set_high()
    }

    #[inline(always)]
    fn set_state(&mut self, state: PinState) -> Result<(), Self::Error> {
        self.0.set_state(state)
    }
}

/// An `OutputPin` wrapper that inverts LOW to HIGH and vice versa.
pub struct InvertedPin<I>(pub I);

impl<I> OutputPin for InvertedPin<I>
where
    I: OutputPin,
{
    type Error = I::Error;

    #[inline(always)]
    fn set_low(&mut self) -> Result<(), Self::Error> {
        self.0.set_high()
    }

    #[inline(always)]
    fn set_high(&mut self) -> Result<(), Self::Error> {
        self.0.set_low()
    }

    #[inline(always)]
    fn set_state(&mut self, state: PinState) -> Result<(), Self::Error> {
        self.0.set_state(!state)
    }
}

/// An `OutputPin` that applies changes to all the `OutputPin`s inside.
pub struct PinGroup<I, const LEN: usize>(pub [I; LEN]);

impl<I, const LEN: usize> OutputPin for PinGroup<I, LEN>
where
    I: OutputPin,
{
    type Error = I::Error;

    fn set_low(&mut self) -> Result<(), Self::Error> {
        for pin in self.0.iter_mut() {
            pin.set_low()?;
        }
        Ok(())
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        for pin in self.0.iter_mut() {
            pin.set_high()?;
        }
        Ok(())
    }

    fn set_state(&mut self, state: PinState) -> Result<(), Self::Error> {
        for pin in self.0.iter_mut() {
            pin.set_state(state)?;
        }
        Ok(())
    }
}

pub struct Syncify<T>(pub T);

impl<T> Syncify<T> {
    pub const unsafe fn new(inner: T) -> Self {
        Self(inner)
    }
}

unsafe impl<T> Sync for Syncify<T> {}
