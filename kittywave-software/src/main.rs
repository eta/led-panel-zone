#![no_std]
#![no_main]

use core::panic::PanicInfo;
use dmg1083::{PanelData, SCAN_LINES};
use dmg1083_software::Dmg1083;
use embedded_graphics::image::Image;
use embedded_graphics_core::geometry::Point;
use embedded_graphics_core::Drawable;
use embedded_hal::digital::v2::OutputPin;
use mbi5153::{Mbi5153Config, Mbi5153Config2};
use misc_hacks::{InvertedPin, PinGroup, PinRef};
use rp_pico::hal::Clock;
use rp_pico::{entry, hal, pac, Pins};
use rtt_target::{rprintln, rtt_init_print};
use tinybmp::Bmp;

const KITTYWAVE: &[u8] = include_bytes!("../kittywave.bmp");

#[panic_handler]
fn panic_handler(i: &PanicInfo) -> ! {
    rprintln!("{}", i);
    loop {}
}

#[entry]
fn main() -> ! {
    rtt_init_print!();

    rprintln!("[+] welcome to the LED Panel Zone! setting up...");

    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // The delay object lets us wait for specified amounts of time (in
    // milliseconds)
    let _delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    // The single-cycle I/O block controls our GPIO pins
    let sio = hal::Sio::new(pac.SIO);

    // Set the pins up according to their function on this particular board
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Address lines
    let addr_a = pins.gpio2.into_push_pull_output().into_dyn_pin();
    let addr_b = pins.gpio3.into_push_pull_output().into_dyn_pin();
    let addr_c = pins.gpio4.into_push_pull_output().into_dyn_pin();
    let addr_d = pins.gpio5.into_push_pull_output().into_dyn_pin();
    let addr_e = pins.gpio6.into_push_pull_output().into_dyn_pin();

    // Clocks
    let gclk = pins.gpio7.into_push_pull_output().into_dyn_pin();
    let mut dclk = pins.gpio8.into_push_pull_output().into_dyn_pin();
    let mut latch = pins.gpio9.into_push_pull_output().into_dyn_pin();

    // Data lines
    let mut r1_sdi = pins.gpio10.into_push_pull_output().into_dyn_pin();
    let mut g1_sdi = pins.gpio11.into_push_pull_output().into_dyn_pin();
    let mut b1_sdi = pins.gpio12.into_push_pull_output().into_dyn_pin();
    let mut r2_sdi = pins.gpio13.into_push_pull_output().into_dyn_pin();
    let mut g2_sdi = pins.gpio14.into_push_pull_output().into_dyn_pin();
    let mut b2_sdi = pins.gpio15.into_push_pull_output().into_dyn_pin();
    // note schematic fuckery:
    // G3 actually controls R3
    // (R3 actually controls G3)
    // B4 actually controls B3
    // (B3 actually controls B4)
    // G4 actually controls R4
    // (R4 actually controls G4)
    let mut r3_sdi = pins.gpio17.into_push_pull_output().into_dyn_pin();
    let mut g3_sdi = pins.gpio16.into_push_pull_output().into_dyn_pin();
    let mut b3_sdi = pins.gpio21.into_push_pull_output().into_dyn_pin();
    let mut r4_sdi = pins.gpio20.into_push_pull_output().into_dyn_pin();
    let mut g4_sdi = pins.gpio19.into_push_pull_output().into_dyn_pin();
    let mut b4_sdi = pins.gpio18.into_push_pull_output().into_dyn_pin();
    // (end schematic fuckery)

    // SR pin that needs to be low to work
    let mut sr = pins.gpio22.into_push_pull_output().into_dyn_pin();
    sr.set_high().unwrap();

    rprintln!("[+] configuring panel");

    let sdis = PinGroup([
        InvertedPin(PinRef(&mut r1_sdi)),
        InvertedPin(PinRef(&mut r2_sdi)),
        InvertedPin(PinRef(&mut r3_sdi)),
        InvertedPin(PinRef(&mut r4_sdi)),
        InvertedPin(PinRef(&mut g1_sdi)),
        InvertedPin(PinRef(&mut g2_sdi)),
        InvertedPin(PinRef(&mut g3_sdi)),
        InvertedPin(PinRef(&mut g4_sdi)),
        InvertedPin(PinRef(&mut b1_sdi)),
        InvertedPin(PinRef(&mut b2_sdi)),
        InvertedPin(PinRef(&mut b3_sdi)),
        InvertedPin(PinRef(&mut b4_sdi)),
    ]);

    let config = Mbi5153Config {
        scan_line_count: SCAN_LINES as _,
        current_gain: 255,
        gclk_multiplier: true,
        lower_ghost_elimination: true,
        ..Default::default()
    };

    dmg1083::configure_panel(
        config,
        InvertedPin(PinRef(&mut dclk)),
        InvertedPin(PinRef(&mut latch)),
        sdis,
    )
    .unwrap();

    dmg1083::configure_panel_2(
        InvertedPin(PinRef(&mut dclk)),
        InvertedPin(PinRef(&mut latch)),
        Mbi5153Config2::appnote_secret_sauce_r(),
        PinGroup([
            InvertedPin(PinRef(&mut r1_sdi)),
            InvertedPin(PinRef(&mut r2_sdi)),
            InvertedPin(PinRef(&mut r3_sdi)),
            InvertedPin(PinRef(&mut r4_sdi)),
        ]),
        Mbi5153Config2::appnote_secret_sauce_gb(),
        PinGroup([
            InvertedPin(PinRef(&mut g1_sdi)),
            InvertedPin(PinRef(&mut g2_sdi)),
            InvertedPin(PinRef(&mut g3_sdi)),
            InvertedPin(PinRef(&mut g4_sdi)),
        ]),
        Mbi5153Config2::appnote_secret_sauce_gb(),
        PinGroup([
            InvertedPin(PinRef(&mut b1_sdi)),
            InvertedPin(PinRef(&mut b2_sdi)),
            InvertedPin(PinRef(&mut b3_sdi)),
            InvertedPin(PinRef(&mut b4_sdi)),
        ]),
    )
    .unwrap();
    rprintln!("[+] initialising software driver");

    let mut app = Dmg1083::new(
        dclk,
        gclk,
        latch,
        [
            r1_sdi, g1_sdi, b1_sdi, r2_sdi, g2_sdi, b2_sdi, r3_sdi, g3_sdi, b3_sdi, r4_sdi, g4_sdi,
            b4_sdi,
        ],
        [addr_a, addr_b, addr_c, addr_d, addr_e],
        sio.fifo,
        &mut pac.PSM,
        &mut pac.PPB,
        config.gclk_multiplier,
    );

    rprintln!("[+] sending frames");

    // SENDING DATA
    let mut panel: PanelData;

    let bmp = Bmp::from_slice(KITTYWAVE).unwrap();

    loop {
        panel = PanelData::default();
        Image::new(&bmp, Point::new(2, 0)).draw(&mut panel).unwrap();
        app.transmit_frame(&panel);
        app.vsync();
    }
}
