#![no_std]
use core::mem;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct QoiHeader {
    pub width: u32,
    pub height: u32,
    pub channels: u8,
    pub colour_space: u8,
}

#[derive(Default, Debug, Clone)]
pub struct StreamingQoiDecoder {
    inner: ParserState,
}

impl StreamingQoiDecoder {
    pub fn new() -> Self {
        Self {
            inner: Default::default(),
        }
    }

    pub fn apply<'a>(
        &mut self,
        data: &'a [u8],
    ) -> Result<(u32, u32, Rgba8, &'a [u8]), ParserError> {
        self.inner.apply(data)
    }
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone)]
enum ParserState {
    Header {
        bytes: [u8; 14],
        idx: usize,
        poisoned: bool,
    },
    Pixels {
        chunk: [u8; 5],
        filled: usize,
        pixel_count: u32,
        run_length: u8,
        last: Rgba8,
        previous: [Rgba8; 64],
        header: QoiHeader,
    },
}

impl Default for ParserState {
    fn default() -> Self {
        Self::Header {
            bytes: [0; 14],
            idx: 0,
            poisoned: false,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Rgba8 {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Rgba8 {
    fn as_qoi_hash(&self) -> u8 {
        self.r
            .wrapping_mul(3)
            .wrapping_add(self.g.wrapping_mul(5))
            .wrapping_add(self.b.wrapping_mul(7))
            .wrapping_add(self.a.wrapping_mul(11))
            % 64
    }
}

impl Default for Rgba8 {
    fn default() -> Self {
        Self {
            r: 0,
            g: 0,
            b: 0,
            a: 255,
        }
    }
}

impl From<(u8, u8, u8, u8)> for Rgba8 {
    fn from((r, g, b, a): (u8, u8, u8, u8)) -> Self {
        Self { r, g, b, a }
    }
}

pub enum ParserError {
    Incomplete,
    Complete,
    BadMagic,
}

impl ParserState {
    fn apply<'a>(
        &mut self,
        mut data: &'a [u8],
    ) -> Result<(u32, u32, Rgba8, &'a [u8]), ParserError> {
        assert!(!data.is_empty());

        match self {
            ParserState::Header {
                bytes,
                idx,
                poisoned,
            } => {
                // We're reading a header into `bytes`. `idx` stores
                // the current fill pointer.

                if *poisoned {
                    return Err(ParserError::BadMagic);
                }

                let bytes_left = 14 - *idx;
                debug_assert_ne!(bytes_left, 0);

                if data.len() >= bytes_left {
                    bytes[*idx..].copy_from_slice(&data[..bytes_left]);

                    if bytes[0..4] != [b'q', b'o', b'i', b'f'] {
                        *poisoned = true;
                        return Err(ParserError::BadMagic);
                    }

                    let header = QoiHeader {
                        width: u32::from_be_bytes(bytes[4..8].try_into().unwrap()),
                        height: u32::from_be_bytes(bytes[8..12].try_into().unwrap()),
                        channels: bytes[12],
                        colour_space: bytes[13],
                    };

                    mem::swap(
                        self,
                        &mut ParserState::Pixels {
                            chunk: [0; 5],
                            filled: 0,
                            pixel_count: 0,
                            last: Rgba8::default(),
                            previous: [Rgba8::default(); 64],
                            header,
                            run_length: 0,
                        },
                    );

                    if data.len() == bytes_left {
                        // If we don't have any left over, return.
                        Err(ParserError::Incomplete)
                    } else {
                        // Otherwise, start parsing the data.
                        self.apply(&data[bytes_left..])
                    }
                } else {
                    bytes[*idx..(*idx + data.len())].copy_from_slice(data);
                    *idx += data.len();
                    Err(ParserError::Incomplete)
                }
            }
            ParserState::Pixels {
                chunk,
                filled,
                pixel_count,
                last,
                previous,
                header,
                run_length,
            } => {
                let mut fill_one = |_chunk: &mut [u8], _filled: &mut usize| {
                    let (head, rest) = data.split_first().ok_or(ParserError::Incomplete)?;
                    data = rest;
                    _chunk[*_filled] = *head;
                    *_filled += 1;
                    Ok(())
                };

                if *pixel_count >= header.height * header.width {
                    return Err(ParserError::Complete);
                }

                if *run_length > 0 {
                    *run_length -= 1;
                } else {
                    if *filled == 0 {
                        fill_one(chunk, filled)?;
                    }

                    match chunk[0] & 0b11_000000 {
                        0b00_000000 /* QOI_OP_INDEX */ => {
                            *last = previous[chunk[0] as usize];
                        },
                        0b01_000000 /* QOI_OP_DIFF */ => {
                            let dr = (chunk[0] & 0b00_11_00_00) >> 4;
                            let dg = (chunk[0] & 0b00_00_11_00) >> 2;
                            let db = chunk[0] & 0b00_00_00_11;

                            last.r = last.r.wrapping_add(dr).wrapping_sub(2);
                            last.g = last.g.wrapping_add(dg).wrapping_sub(2);
                            last.b = last.b.wrapping_add(db).wrapping_sub(2);
                        },
                        0b10_000000 /* QOI_OP_LUMA */ => {
                            while *filled < 2 {
                                fill_one(chunk, filled)?;
                            }

                            let dg = (chunk[0] & 0b00_111111).wrapping_sub(32);
                            let dr = (chunk[1] >> 4).wrapping_sub(8).wrapping_add(dg);
                            let db = (chunk[1] & 0b0000_1111).wrapping_sub(8).wrapping_add(dg);

                            last.r = last.r.wrapping_add(dr);
                            last.g = last.g.wrapping_add(dg);
                            last.b = last.b.wrapping_add(db);
                        },
                        0b11_000000 => match chunk[0] {
                            0b11111110 /* QOI_OP_RGB */ => {
                                while *filled < 4 {
                                    fill_one(chunk, filled)?;
                                }

                                last.r = chunk[1];
                                last.g = chunk[2];
                                last.b = chunk[3];
                            },
                            0b11111111 /* QOI_OP_RGBA */ => {
                                while *filled < 5 {
                                    fill_one(chunk, filled)?;
                                }

                                last.r = chunk[1];
                                last.g = chunk[2];
                                last.b = chunk[3];
                                last.a = chunk[4];
                            },
                            _ /* QOI_OP_RUN */ => {
                                *run_length = chunk[0] & 0b00_111111;
                            }
                        }
                        _ => unreachable!()
                    }
                    *filled = 0;
                    previous[last.as_qoi_hash() as usize] = *last;
                }

                let (x, y) = (*pixel_count % header.width, *pixel_count / header.width);
                *pixel_count += 1;
                Ok((x, y, *last, data))
            }
        }
    }
}

#[cfg(test)]
extern crate alloc;
#[cfg(test)]
extern crate std;

#[cfg(test)]
mod tests {
    use super::*;
    use alloc::vec;
    use alloc::vec::Vec;
    use embedded_graphics_core::prelude::RgbColor;
    use std::eprintln;

    static SQUISH_QOI: &[u8] = include_bytes!("../tests/squish.qoi");

    #[test]
    fn squish_vs_tinyqoi() {
        let tinyqoi = tinyqoi::Qoi::new(SQUISH_QOI).unwrap();
        let mut tinyqoi_iter = tinyqoi.pixels();

        let mut our_ptr = SQUISH_QOI as &[u8];
        let mut our_state = ParserState::default();
        while let Ok((x, y, our_pixel, rest)) = our_state.apply(our_ptr) {
            our_ptr = rest;
            let their_pixel = tinyqoi_iter.next().expect("tinyqoi ran out");
            eprintln!("({x}, {y}) = {our_pixel:?}");
            assert_eq!(
                (our_pixel.r, our_pixel.g, our_pixel.b),
                (their_pixel.r(), their_pixel.g(), their_pixel.b())
            );
        }
        assert!(tinyqoi_iter.next().is_none());
    }

    #[test]
    fn chunks() {
        fn apply_with_chunk_size(size: usize) -> Vec<Rgba8> {
            let mut ret = vec![];
            let mut state = ParserState::default();
            for mut chunk in SQUISH_QOI.chunks(size) {
                while let Ok((x, y, pixel, rest)) = state.apply(chunk) {
                    chunk = rest;
                    ret.push(pixel);
                    if rest.is_empty() {
                        break;
                    }
                }
            }
            ret
        }
        let original = apply_with_chunk_size(SQUISH_QOI.len());
        for n in 1..80 {
            assert_eq!(original, apply_with_chunk_size(n));
        }
    }
}
