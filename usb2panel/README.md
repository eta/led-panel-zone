# usb2panel

Firmware for controlling a driver board via an attached computer over USB.

## USB protocol

The board enumerates as a USB 1.0 Full Speed device with VID:PID `1209:3706`. The serial number reported over USB
is unique per board.

Relevant endpoints:

- Endpoint 1 OUT (bulk): `ENDPOINT_DATA_OUT`, used for images
- Endpoint 2 OUT (interrupt): `ENDPOINT_CMD_OUT`, used for vsync
- Endpoint 1 IN (interrupt): `ENDPOINT_REPLY_IN`, used for per-frame status updates

To send a frame, write a [QOI](https://qoiformat.org/)-encoded image to `ENDPOINT_DATA_OUT`, then write 0x01 to `ENDPOINT_CMD_OUT`
when you want the frame to be displayed (i.e. trigger a vsync). Then, read 1 byte from `ENDPOINT_REPLY_IN` to determine the result.

You will get `101` (`REPLY_DONE`) if the frame was displayed, `102` (`REPLY_BAD_MAGIC`) if the image did not contain a valid QOI
header, or `103` (`REPLY_BAD_PIXEL`) if the image was larger than 78 by 78 pixels.

You can then send another frame by repeating this whole procedure.

