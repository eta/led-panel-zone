use crate::core1::{MAX_QOI_LEN, MSG_ABORT, MSG_DECODE, QOI_QUEUE, REPLY_ABORTED};
use crate::queue::QueueProducer;
use embedded_hal::digital::v2::OutputPin;
use rp2040_hal::gpio::bank0::{Gpio24, Gpio25};
use rp2040_hal::gpio::{FunctionSioOutput, Pin, PullDown};
use rp2040_hal::sio::SioFifo;
use rtt_target::{rprint, rprintln};
use usb_device::bus::{InterfaceNumber, UsbBus, UsbBusAllocator};
use usb_device::class::UsbClass;
use usb_device::class_prelude::{DescriptorWriter, EndpointAddress, StringIndex};
use usb_device::device::DEFAULT_ALTERNATE_SETTING;
use usb_device::endpoint::{EndpointIn, EndpointOut};
use usb_device::UsbError;

const PACKET_SIZE: usize = 64;

pub struct PanelClass<'a, B: UsbBus> {
    iface: InterfaceNumber,
    iface_string: StringIndex,
    ep_data_out: EndpointOut<'a, B>,
    ep_request_out: EndpointOut<'a, B>,
    ep_request_in: EndpointIn<'a, B>,
    state: State,
    fifo: SioFifo,
    fifo_result: Option<u32>,
    producer: Option<QueueProducer<'static, MAX_QOI_LEN>>,
    data_available: bool,
    request_available: bool,
    led_b: Pin<Gpio24, FunctionSioOutput, PullDown>,
    led_c: Pin<Gpio25, FunctionSioOutput, PullDown>,
}

#[derive(Debug, Copy, Clone)]
enum State {
    Receiving,
    WaitingForDecoder,
    Flipping,
}

impl<'a, B: UsbBus> PanelClass<'a, B> {
    pub(crate) fn new(
        alloc: &'a UsbBusAllocator<B>,
        fifo: SioFifo,
        led_b: Pin<Gpio24, FunctionSioOutput, PullDown>,
        led_c: Pin<Gpio25, FunctionSioOutput, PullDown>,
    ) -> Self {
        Self {
            iface: alloc.interface(),
            iface_string: alloc.string(),
            ep_data_out: alloc.bulk(PACKET_SIZE as _),
            ep_request_out: alloc.interrupt(1, 1),
            ep_request_in: alloc.interrupt(1, 1),
            producer: None,
            fifo,
            fifo_result: None,
            data_available: false,
            request_available: false,
            state: State::Receiving,
            led_b,
            led_c,
        }
    }

    pub fn take_pending_flip(&mut self) -> bool {
        if matches!(self.state, State::Flipping) {
            self.state = State::Receiving;
            true
        } else {
            false
        }
    }
}

impl<'a, B: UsbBus> UsbClass<B> for PanelClass<'a, B> {
    fn get_configuration_descriptors(
        &self,
        writer: &mut DescriptorWriter,
    ) -> usb_device::Result<()> {
        writer.interface_alt(
            self.iface,
            DEFAULT_ALTERNATE_SETTING,
            0xff,
            0x00,
            0x00,
            Some(self.iface_string),
        )?;
        writer.endpoint(&self.ep_data_out)?;
        writer.endpoint(&self.ep_request_out)?;
        writer.endpoint(&self.ep_request_in)?;
        Ok(())
    }

    fn reset(&mut self) {
        rprint!("usb: resetting...");
        self.fifo.write_blocking(MSG_ABORT);
        loop {
            if self.fifo.read_blocking() == REPLY_ABORTED {
                break;
            }
        }
        self.state = State::Receiving;
        self.producer = None;
        self.ep_data_out.unstall();
        self.led_c.set_high().unwrap();
        rprintln!("done");
    }

    fn poll(&mut self) {
        match self.state {
            State::Receiving => {
                if self.data_available {
                    self.data_available = false;
                    self.led_c.set_low().unwrap();
                    if self.producer.is_none() {
                        self.fifo.write_blocking(MSG_DECODE);
                        self.producer = Some(QOI_QUEUE.produce());
                        self.led_b.set_high().unwrap();
                    }
                    let producer = self.producer.as_mut().unwrap();
                    producer.write(|buf| match self.ep_data_out.read(buf) {
                        Ok(count) => count,
                        Err(UsbError::WouldBlock) => 0,
                        Err(UsbError::BufferOverflow) => {
                            let mut temp_buf = [0u8; PACKET_SIZE];
                            let mut overflow_size = 0usize;
                            while let Ok(sz) = self.ep_data_out.read(&mut temp_buf) {
                                overflow_size += sz;
                            }
                            rprintln!(
                                "usb: overflowed by {} bytes; consider increasing MAX_QOI_LEN!",
                                overflow_size
                            );
                            0
                        }
                        Err(e) => unreachable!("ep_data_out read failed: {e:?}"),
                    });
                }

                if self.request_available {
                    self.request_available = false;
                    let mut buf = [0u8; 1];
                    match self.ep_request_out.read(&mut buf) {
                        Ok(c) => {
                            assert_eq!(c, 1);
                            self.state = State::WaitingForDecoder;
                        }
                        Err(UsbError::WouldBlock) => {} // hmm, that shouldn't really happen...
                        // this is an overflow otherwise, or something not documented
                        Err(e) => unreachable!("ep_request_out read failed: {e:?}"),
                    }
                }
            }
            State::WaitingForDecoder => {
                if let Some(m) = self.fifo.read().or(self.fifo_result.take()) {
                    self.led_b.set_low().unwrap();
                    match self.ep_request_in.write(&[m as u8]) {
                        Ok(_) => {
                            self.producer = None;
                            self.state = State::Flipping;
                        }
                        Err(UsbError::WouldBlock) => {
                            rprintln!("blocking on ep_request_in, grr");
                            self.fifo_result = Some(m);
                        }
                        Err(e) => panic!("ep_request_in write failed: {e:?}"),
                    }
                }
            }
            State::Flipping => {}
        }
    }

    fn endpoint_out(&mut self, addr: EndpointAddress) {
        if addr == self.ep_data_out.address() {
            self.data_available = true;
        }
        if addr == self.ep_request_out.address() {
            self.request_available = true;
        }
    }

    fn get_string(&self, index: StringIndex, _lang_id: u16) -> Option<&str> {
        if index == self.iface_string {
            Some("packet data")
        } else {
            None
        }
    }
}
