#![no_std]
#![no_main]

use crate::frameflip::DOUBLE_BUFFER;
use crate::tinystr::TinyString;
use crate::usb::PanelClass;
use core::fmt::Write;
use core::panic::PanicInfo;
use dmg1083::SCAN_LINES;
use dmg1083_pio::Dmg1083;
use embedded_hal::digital::v2::OutputPin;
use mbi5153::{Mbi5153Config, Mbi5153Config2};
use misc_hacks::{PinGroup, PinRef};
use rp2040_hal::clocks::ClocksManager;
use rp2040_hal::dma::DMAExt;
use rp2040_hal::fugit::{HertzU32, RateExtU32};
use rp2040_hal::gpio::Pins;
use rp2040_hal::multicore::{Multicore, Stack};
use rp2040_hal::pll::PLLConfig;
use rp2040_hal::{pac, pll, xosc, Sio, Watchdog};
use rtt_target::{rprint, rprintln, rtt_init_print};
use usb_device::class::UsbClass;
use usb_device::class_prelude::UsbBusAllocator;
use usb_device::prelude::{UsbDeviceBuilder, UsbVidPid};

mod core1;
mod frameflip;
mod mutex;
mod queue;
mod tinystr;
mod usb;

/// The linker will place this boot block at the start of our program image. We
/// need this to help the ROM bootloader get our code up and running.
/// Note: This boot block is not necessary when using a rp-hal based BSP
/// as the BSPs already perform this step.
#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER_W25Q080;

/// External high-speed crystal on the Raspberry Pi Pico board is 12 MHz. Adjust
/// if your board has a different frequency
const XTAL_FREQ_HZ: u32 = 12_000_000u32;

#[panic_handler]
fn panic_handler(i: &PanicInfo) -> ! {
    rprintln!("{}", i);
    loop {}
}

static mut CORE1_STACK: Stack<4096> = Stack::new();

#[rp2040_hal::entry]
fn main() -> ! {
    rtt_init_print!();

    let mut unique_id = [0u8; 8];

    critical_section::with(|_| unsafe {
        rp2040_flash::flash::flash_unique_id(&mut unique_id, false)
    });

    rprintln!("usb zone");

    let mut unique_id_str = TinyString::<20>::new();

    for b in unique_id {
        write!(&mut unique_id_str, "{:02x}", b).unwrap();
    }

    rprintln!("my serial number is {}", &*unique_id_str);

    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let _core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = Watchdog::new(pac.WATCHDOG);

    // Initialise system clocks
    let xosc = xosc::setup_xosc_blocking(pac.XOSC, XTAL_FREQ_HZ.Hz()).unwrap();

    // Configure watchdog tick generation to tick over every microsecond
    watchdog.enable_tick_generation((XTAL_FREQ_HZ / 1_000_000) as u8);

    let mut clocks = ClocksManager::new(pac.CLOCKS);

    rprint!("configuring clocks...");

    let pll_sys = pll::setup_pll_blocking(
        pac.PLL_SYS,
        xosc.operating_frequency(),
        PLLConfig {
            vco_freq: HertzU32::MHz(1500),
            refdiv: 1,
            post_div1: 6,
            post_div2: 1,
        },
        &mut clocks,
        &mut pac.RESETS,
    )
    .unwrap();

    let pll_usb = pll::setup_pll_blocking(
        pac.PLL_USB,
        xosc.operating_frequency(),
        pll::common_configs::PLL_USB_48MHZ,
        &mut clocks,
        &mut pac.RESETS,
    )
    .unwrap();

    clocks.init_default(&xosc, &pll_sys, &pll_usb).unwrap();

    rprintln!("done");

    // The single-cycle I/O block controls our GPIO pins
    let mut sio = Sio::new(pac.SIO);

    // Set the pins up according to their function on this particular board
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut mc = Multicore::new(&mut pac.PSM, &mut pac.PPB, &mut sio.fifo);
    mc.cores()[1]
        .spawn(unsafe { &mut CORE1_STACK.mem }, core1::core1_main)
        .unwrap();

    let led_b = pins.gpio24.into_push_pull_output();
    let led_c = pins.gpio25.into_push_pull_output();

    let usb_bus = UsbBusAllocator::new(rp2040_hal::usb::UsbBus::new(
        pac.USBCTRL_REGS,
        pac.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut pac.RESETS,
    ));

    let mut panel_class = PanelClass::new(&usb_bus, sio.fifo, led_b, led_c);

    let mut usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x1209, 0x3706))
        .manufacturer("eta")
        .product("DMG1083 panel driver")
        .serial_number(&unique_id_str)
        .device_class(0xff) // vendor specific from: https://www.usb.org/defined-class-codes
        .build();

    // Address lines
    let addr_a = pins.gpio2.into_push_pull_output().into_dyn_pin();
    let addr_b = pins.gpio3.into_push_pull_output().into_dyn_pin();
    let addr_c = pins.gpio4.into_push_pull_output().into_dyn_pin();
    let addr_d = pins.gpio5.into_push_pull_output().into_dyn_pin();
    let addr_e = pins.gpio6.into_push_pull_output().into_dyn_pin();

    // Clocks
    let gclk = pins.gpio7.into_push_pull_output().into_dyn_pin();
    let mut dclk = pins.gpio8.into_push_pull_output().into_dyn_pin();
    let mut latch = pins.gpio9.into_push_pull_output().into_dyn_pin();

    // Data lines
    let mut r1_sdi = pins.gpio10.into_push_pull_output().into_dyn_pin();
    let mut g1_sdi = pins.gpio11.into_push_pull_output().into_dyn_pin();
    let mut b1_sdi = pins.gpio12.into_push_pull_output().into_dyn_pin();

    let mut r2_sdi = pins.gpio13.into_push_pull_output().into_dyn_pin();
    let mut g2_sdi = pins.gpio14.into_push_pull_output().into_dyn_pin();
    let mut b2_sdi = pins.gpio15.into_push_pull_output().into_dyn_pin();

    let mut r3_sdi = pins.gpio16.into_push_pull_output().into_dyn_pin();
    let mut g3_sdi = pins.gpio17.into_push_pull_output().into_dyn_pin();
    let mut b3_sdi = pins.gpio18.into_push_pull_output().into_dyn_pin();

    let mut r4_sdi = pins.gpio19.into_push_pull_output().into_dyn_pin();
    let mut g4_sdi = pins.gpio20.into_push_pull_output().into_dyn_pin();
    let mut b4_sdi = pins.gpio21.into_push_pull_output().into_dyn_pin();

    // SR pin that needs to be low to work
    let mut sr = pins.gpio22.into_push_pull_output().into_dyn_pin();
    sr.set_low().unwrap();

    // shift register enable pin
    let mut shift_en = pins.gpio1.into_push_pull_output().into_dyn_pin();
    shift_en.set_low().unwrap();

    let mut led_a = pins.gpio23.into_push_pull_output();

    rprintln!("configuring panel...");

    let sdis = PinGroup([
        PinRef(&mut r1_sdi),
        PinRef(&mut r2_sdi),
        PinRef(&mut r3_sdi),
        PinRef(&mut r4_sdi),
        PinRef(&mut g1_sdi),
        PinRef(&mut g2_sdi),
        PinRef(&mut g3_sdi),
        PinRef(&mut g4_sdi),
        PinRef(&mut b1_sdi),
        PinRef(&mut b2_sdi),
        PinRef(&mut b3_sdi),
        PinRef(&mut b4_sdi),
    ]);

    let config = Mbi5153Config {
        scan_line_count: SCAN_LINES as _,
        gclk_multiplier: true,
        lower_ghost_elimination: true,
        current_gain: u8::MAX,
        ..Default::default()
    };

    dmg1083::configure_panel(config, PinRef(&mut dclk), PinRef(&mut latch), sdis).unwrap();

    dmg1083::configure_panel_2(
        PinRef(&mut dclk),
        PinRef(&mut latch),
        Mbi5153Config2::appnote_secret_sauce_r(),
        PinGroup([
            PinRef(&mut r1_sdi),
            PinRef(&mut r2_sdi),
            PinRef(&mut r3_sdi),
            PinRef(&mut r4_sdi),
        ]),
        Mbi5153Config2::appnote_secret_sauce_gb(),
        PinGroup([
            PinRef(&mut g1_sdi),
            PinRef(&mut g2_sdi),
            PinRef(&mut g3_sdi),
            PinRef(&mut g4_sdi),
        ]),
        Mbi5153Config2::appnote_secret_sauce_gb(),
        PinGroup([
            PinRef(&mut b1_sdi),
            PinRef(&mut b2_sdi),
            PinRef(&mut b3_sdi),
            PinRef(&mut b4_sdi),
        ]),
    )
    .unwrap();

    rprintln!("configuring PIO...");

    // FIXME(eta): You know, I'm not sure the typestates are actually helping.

    let mut panel = Dmg1083::new_pio0(
        pac.PIO0,
        &mut pac.RESETS,
        false, // should_invert
        config.gclk_multiplier,
        dclk.try_into_function().ok().unwrap(),
        gclk.try_into_function().ok().unwrap(),
        latch.try_into_function().ok().unwrap(),
        [
            r1_sdi.try_into_function().ok().unwrap(),
            g1_sdi.try_into_function().ok().unwrap(),
            b1_sdi.try_into_function().ok().unwrap(),
            r2_sdi.try_into_function().ok().unwrap(),
            g2_sdi.try_into_function().ok().unwrap(),
            b2_sdi.try_into_function().ok().unwrap(),
            r3_sdi.try_into_function().ok().unwrap(),
            g3_sdi.try_into_function().ok().unwrap(),
            b3_sdi.try_into_function().ok().unwrap(),
            r4_sdi.try_into_function().ok().unwrap(),
            g4_sdi.try_into_function().ok().unwrap(),
            b4_sdi.try_into_function().ok().unwrap(),
        ],
        [
            addr_a.try_into_function().ok().unwrap(),
            addr_b.try_into_function().ok().unwrap(),
            addr_c.try_into_function().ok().unwrap(),
            addr_d.try_into_function().ok().unwrap(),
            addr_e.try_into_function().ok().unwrap(),
        ],
    );

    rprintln!("blanking");

    let dma = pac.DMA.split(&mut pac.RESETS);

    DOUBLE_BUFFER.lock().give_dma(dma.ch0);

    // The buffers start out zeroed; we need to flip twice to clear both SRAMs inside the IC
    DOUBLE_BUFFER.lock().flip(&mut panel);
    DOUBLE_BUFFER.lock().flip(&mut panel);
    DOUBLE_BUFFER.lock().flush(&mut panel);

    rprintln!("zone");

    loop {
        usb_dev.poll(&mut [&mut panel_class]);
        // NOTE(eta): This is required to continue checking the FIFO even when there's no USB
        //            traffic!!!
        panel_class.poll();
        if panel_class.take_pending_flip() {
            led_a.set_high().unwrap();
            DOUBLE_BUFFER.lock().flip(&mut panel);
            led_a.set_low().unwrap();
        }
    }
}
