//! A small stack-allocated string thingie just used for the unique ID

use core::fmt;
use core::ops::Deref;

pub struct TinyString<const N: usize> {
    inner: [u8; N],
    len: usize,
}

#[allow(clippy::new_without_default)]
impl<const N: usize> TinyString<N> {
    pub fn new() -> Self {
        TinyString {
            inner: [0; N],
            len: 0,
        }
    }
}

impl<'a, const N: usize> fmt::Write for &'a mut TinyString<N> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let bytes = s.as_bytes();
        if bytes.len() + self.len >= N {
            return Err(fmt::Error);
        }
        for ch in s.as_bytes() {
            self.inner[self.len] = *ch;
            self.len += 1;
        }
        Ok(())
    }
}

impl<const N: usize> Deref for TinyString<N> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        // SAFETY: we only put in valid stuff anyway
        unsafe { core::str::from_utf8_unchecked(&self.inner[..self.len]) }
    }
}
