//! Mutex??

use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut};
use rp2040_hal::sio::{Spinlock, SpinlockValid};

pub struct PicoMutex<T, const N: usize>
where
    Spinlock<N>: SpinlockValid,
{
    data: UnsafeCell<T>,
}

unsafe impl<T, const N: usize> Sync for PicoMutex<T, N> where Spinlock<N>: SpinlockValid {}

impl<T, const N: usize> PicoMutex<T, N>
where
    Spinlock<N>: SpinlockValid,
{
    pub const fn new(data: T) -> Self {
        Self {
            data: UnsafeCell::new(data),
        }
    }
    pub fn lock(&self) -> PicoMutexGuard<'_, T, N> {
        PicoMutexGuard {
            inner: self,
            _lock: Spinlock::<N>::claim(),
        }
    }
}

pub struct PicoMutexGuard<'a, T, const N: usize>
where
    Spinlock<N>: SpinlockValid,
{
    inner: &'a PicoMutex<T, N>,
    _lock: Spinlock<N>,
}

impl<'a, T, const N: usize> Deref for PicoMutexGuard<'a, T, N>
where
    Spinlock<N>: SpinlockValid,
{
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.inner.data.get() }
    }
}

impl<'a, T, const N: usize> DerefMut for PicoMutexGuard<'a, T, N>
where
    Spinlock<N>: SpinlockValid,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.inner.data.get() }
    }
}
