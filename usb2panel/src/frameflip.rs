//! A double buffer for frame data.

use crate::mutex::PicoMutex;
use core::cell::{Ref, RefCell, RefMut};
use core::mem;
use dmg1083::PIO_DATA_LEN;
use dmg1083_pio::Dmg1083;
use misc_hacks::Syncify;
use rp2040_hal::dma::single_buffer::Transfer;
use rp2040_hal::dma::{single_buffer, Channel, CH0};
use rp2040_hal::pac::PIO0;
use rp2040_hal::pio::{Tx, SM0};

enum DmaState {
    Transferring {
        #[allow(clippy::type_complexity)]
        xfer: Transfer<Channel<CH0>, Ref<'static, [u32; PIO_DATA_LEN]>, Tx<(PIO0, SM0)>>,
    },
    Ready {
        channel: Channel<CH0>,
    },
}

impl DmaState {
    fn into_channel(self, panel: &mut Dmg1083<PIO0>) -> Channel<CH0> {
        match self {
            DmaState::Ready { channel } => channel,
            DmaState::Transferring { xfer } => {
                let (channel, bufref, frame_tx) = xfer.wait();
                *panel.frame_tx_mut() = Some(frame_tx);
                mem::drop(bufref);

                panel.vsync();

                channel
            }
        }
    }

    fn transfer(self, buf: Ref<'static, [u32; PIO_DATA_LEN]>, panel: &mut Dmg1083<PIO0>) -> Self {
        let ch0 = self.into_channel(panel);
        let frame_tx = panel.frame_tx_mut().take().unwrap();

        let xfer = single_buffer::Config::new(ch0, buf, frame_tx).start();
        Self::Transferring { xfer }
    }

    fn wait_ready(self, panel: &mut Dmg1083<PIO0>) -> Self {
        Self::Ready {
            channel: self.into_channel(panel),
        }
    }
}

pub struct DoubleBuffer {
    a_is_active: bool,
    dma: Option<DmaState>,
}

// SAFETY: We only access these buffers from one core, inside this module.
//         They unfortunately have to be 'static for the DMA stuff to work.

// TODO(eta): Investigate an alternative solution using pinning, maybe?

static BUFFER_A: Syncify<RefCell<[u32; PIO_DATA_LEN]>> =
    unsafe { Syncify::new(RefCell::new([0; PIO_DATA_LEN])) };

static BUFFER_B: Syncify<RefCell<[u32; PIO_DATA_LEN]>> =
    unsafe { Syncify::new(RefCell::new([0; PIO_DATA_LEN])) };

pub static DOUBLE_BUFFER: PicoMutex<DoubleBuffer, 10> = PicoMutex::new(DoubleBuffer::new());

impl DoubleBuffer {
    pub const fn new() -> Self {
        Self {
            a_is_active: true,
            dma: None,
        }
    }

    pub fn give_dma(&mut self, channel: Channel<CH0>) {
        self.dma = Some(DmaState::Ready { channel })
    }

    pub fn get_mut(&mut self) -> RefMut<'_, [u32; PIO_DATA_LEN]> {
        let buf: RefMut<'static, [u32; PIO_DATA_LEN]> = if self.a_is_active {
            BUFFER_A.0.borrow_mut()
        } else {
            BUFFER_B.0.borrow_mut()
        };
        unsafe { mem::transmute(buf) }
    }

    pub fn flip(&mut self, panel: &mut Dmg1083<PIO0>) {
        let (written, next) = if self.a_is_active {
            (BUFFER_A.0.borrow(), &BUFFER_B)
        } else {
            (BUFFER_B.0.borrow(), &BUFFER_A)
        };
        self.a_is_active = !self.a_is_active;
        self.dma = Some(self.dma.take().unwrap().transfer(written, panel));
        *next.0.borrow_mut() = [0; PIO_DATA_LEN];
    }

    pub fn flush(&mut self, panel: &mut Dmg1083<PIO0>) {
        self.dma = Some(self.dma.take().unwrap().wait_ready(panel));
    }
}
