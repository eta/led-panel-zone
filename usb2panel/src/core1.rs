//! Image decoding on a second core

use crate::frameflip::DOUBLE_BUFFER;
use crate::queue::SimpleQueue;
use dmg1083::PanelDataPio;
use rp2040_hal::sio::SioFifo;
use rp2040_hal::{pac, Sio};
use rtt_target::rprintln;
use streaming_qoi::{ParserError, StreamingQoiDecoder};

pub const MSG_DECODE: u32 = 0;
pub const MSG_ABORT: u32 = 1;

pub const REPLY_DONE: u32 = 101;
pub const REPLY_BAD_MAGIC: u32 = 102;
pub const REPLY_BAD_PIXEL: u32 = 103;
pub const REPLY_ABORTED: u32 = 104;
pub const REPLY_TRUNCATED: u32 = 105;

// worst case size estimate: 14 byte header, 5 bytes per pixel (QOI_OP_RGBA), 8 byte trailer
// => 14 + (5 * 78 * 78) + 8 => 30,442 byte
pub const MAX_QOI_LEN: usize = 30442;

pub static QOI_QUEUE: SimpleQueue<MAX_QOI_LEN> = SimpleQueue::new();

pub fn streaming_decode_one_image(fifo: &mut SioFifo) -> u32 {
    let mut consumer = QOI_QUEUE.consume();
    let mut qoi = StreamingQoiDecoder::new();
    let mut doublebuf = DOUBLE_BUFFER.lock();
    let mut out = doublebuf.get_mut();

    loop {
        while let Some(mut chunk) = match consumer.next_chunk() {
            Ok(x) => x,
            Err(()) => {
                rprintln!("[core1] frame truncated!");
                return REPLY_TRUNCATED;
            }
        } {
            while let Some((x, y, pixel, rest)) = match qoi.apply(chunk) {
                Ok(v) => Some(v),
                Err(ParserError::Incomplete) => None,
                Err(ParserError::Complete) => {
                    return REPLY_DONE;
                }
                Err(ParserError::BadMagic) => {
                    rprintln!("[core1] bad magic! chunk = {:?}", chunk);
                    return REPLY_BAD_MAGIC;
                }
            } {
                {
                    let mut wrapper = PanelDataPio(&mut out);
                    if x >= 78 || y >= 80 {
                        rprintln!("[core1] bad pixel ({}, {})", x, y);
                        return REPLY_BAD_PIXEL;
                    }
                    wrapper.draw_pixel_8((x + 2) as _, y as _, pixel.r, pixel.g, pixel.b);
                }
                if rest.is_empty() {
                    break;
                }
                chunk = rest;
            }
        }
        if let Some(msg) = fifo.read() {
            rprintln!("[core1] abort decode");
            assert_eq!(msg, MSG_ABORT);
            return REPLY_ABORTED;
        }
    }
}

pub fn core1_main() {
    let pac = unsafe { pac::Peripherals::steal() };
    let _core = unsafe { pac::CorePeripherals::steal() };
    let sio = Sio::new(pac.SIO);
    let mut fifo = sio.fifo;

    rprintln!("[core1] online");
    loop {
        let signal = fifo.read_blocking();
        let response = match signal {
            MSG_DECODE => streaming_decode_one_image(&mut fifo),
            MSG_ABORT => REPLY_ABORTED,
            x => unreachable!("[core1] unknown message {x}"),
        };
        fifo.write_blocking(response);
    }
}
