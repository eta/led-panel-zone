//! Sharing data between the RP2040 cores, hopefully safely?

use crate::mutex::PicoMutex;
use core::cell::UnsafeCell;
use core::slice;
use rp2040_hal::sio::Spinlock0;

pub struct SimpleQueue<const CAPACITY: usize> {
    data: UnsafeCell<[u8; CAPACITY]>,
    len: PicoMutex<usize, 1>,
}

impl<const C: usize> SimpleQueue<C> {
    pub const fn new() -> Self {
        Self {
            data: UnsafeCell::new([0; C]),
            len: PicoMutex::new(0),
        }
    }

    // FIXME: what about if this method is called twice?
    pub fn consume(&self) -> QueueConsumer<'_, C> {
        QueueConsumer {
            inner: self,
            _consume_lock: Spinlock0::claim(),
            last_len: 0,
        }
    }

    // FIXME: what about if this method is called twice?
    pub fn produce(&self) -> QueueProducer<'_, C> {
        QueueProducer {
            inner: self,
            last_len: 0,
        }
    }

    fn reset(&self) {
        let _consume_lock = Spinlock0::claim();
        *self.len.lock() = 0;
    }
}

unsafe impl<const C: usize> Sync for SimpleQueue<C> {}

pub struct QueueProducer<'a, const C: usize> {
    inner: &'a SimpleQueue<C>,
    last_len: usize,
}

impl<'a, const C: usize> QueueProducer<'a, C> {
    pub fn write(&mut self, producer: impl FnOnce(&mut [u8]) -> usize) {
        unsafe {
            let write_len = C - self.last_len;
            /*
            if write_len == 0 {
                panic!("QueueProducer::write() called on an empty queue");
            }
             */
            let written = {
                let write_ptr = (self.inner.data.get() as *mut u8).add(self.last_len);
                let write_slice = slice::from_raw_parts_mut(write_ptr, write_len);
                // FIXME(eta): what about panic safety?
                producer(write_slice)
            };
            assert!(written <= write_len);
            self.last_len += written;
            *self.inner.len.lock() = self.last_len;
        }
    }
}

impl<'a, const C: usize> Drop for QueueProducer<'a, C> {
    fn drop(&mut self) {
        self.inner.reset()
    }
}

pub struct QueueConsumer<'a, const C: usize> {
    inner: &'a SimpleQueue<C>,
    _consume_lock: Spinlock0,
    last_len: usize,
}

impl<'a, const C: usize> QueueConsumer<'a, C> {
    /// Get the next chunk, if there is one. An error return means "queue read to end".
    /// A `None` return means "no data yet, but there could be later".
    pub fn next_chunk(&mut self) -> Result<Option<&[u8]>, ()> {
        unsafe {
            let len = *self.inner.len.lock();

            let chunk_start = self.last_len;
            let chunk_len = len - self.last_len;
            if chunk_len == 0 {
                return if self.last_len == C {
                    Err(())
                } else {
                    Ok(None)
                };
            }
            self.last_len = len;

            let data_ptr = self.inner.data.get() as *mut u8;

            Ok(Some(slice::from_raw_parts(
                data_ptr.add(chunk_start).cast_const(),
                chunk_len,
            )))
        }
    }
}
