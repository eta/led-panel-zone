use std::time::{Duration, Instant};

const ENDPOINT_DATA_OUT: u8 = 1;
const ENDPOINT_CMD_OUT: u8 = 2;

const ENDPOINT_REPLY_IN: u8 = 1 | 0x80; /* i.e. endpoint IN 1 */

const CMD_VSYNC: u8 = 1;

const REPLY_DONE: u8 = 101;
const REPLY_BAD_MAGIC: u8 = 102;
const REPLY_BAD_PIXEL: u8 = 103;
const REPLY_ABORTED: u8 = 104;

fn main() {
    println!("opening device!");

    let mut device = rusb::open_device_with_vid_pid(0x1209, 0x3706)
        .expect("couldn't find device with 1209:3706");
    device.reset().expect("couldn't reset device");

    #[cfg(target_os = "macos")]
    {
        println!(
            "setting configuration: {:?}",
            device.set_active_configuration(0)
        );

        device.claim_interface(0).expect("couldn't claim interface");
    }

    println!("opening qois");

    let qois = std::env::args()
        .skip(1)
        .map(|x| std::fs::read(x))
        .collect::<Result<Vec<Vec<_>>, _>>()
        .unwrap();
    if qois.is_empty() {
        eprintln!("no images provided; generate them with `ffmpeg -i SOURCE -pix_fmt rgb24 -vf scale=78:78 DESTINATION/frame%04d.qoi`");
        panic!("give me a list of qois please");
    }

    let start = Instant::now();
    let mut total_frames: u64 = 0;
    let mut second = Instant::now();
    let mut second_frames: u64 = 0;

    let mut write_times = vec![];
    let mut render_times = vec![];

    loop {
        for qoi in qois.iter() {
            let frame_start = Instant::now();
            device
                .write_bulk(ENDPOINT_DATA_OUT, &qoi, Duration::from_millis(250))
                .unwrap();

            let render_start = Instant::now();

            device
                .write_interrupt(ENDPOINT_CMD_OUT, &[CMD_VSYNC], Duration::from_millis(250))
                .unwrap();

            let mut buf = [0u8; 1];
            device
                .read_interrupt(ENDPOINT_REPLY_IN, &mut buf, Duration::from_millis(1000))
                .unwrap();

            eprint!(
                "{}",
                match buf[0] {
                    REPLY_DONE => "✓",
                    REPLY_BAD_MAGIC | REPLY_BAD_PIXEL => "✗",
                    REPLY_ABORTED => "A",
                    _ => "?",
                }
            );

            total_frames += 1;
            second_frames += 1;

            let now = Instant::now();

            write_times.push(render_start.duration_since(frame_start).as_secs_f64());
            render_times.push(now.duration_since(render_start).as_secs_f64());

            if now.duration_since(second) > Duration::from_secs(1) {
                let write_avg: f64 = write_times.iter().sum::<f64>() / write_times.len() as f64;
                let render_avg: f64 = render_times.iter().sum::<f64>() / render_times.len() as f64;

                eprintln!(
                    "\n{}fps ({} frames in {}s, write avg {:.1}ms, render avg {:.1}ms)",
                    second_frames,
                    total_frames,
                    now.duration_since(start).as_secs(),
                    write_avg * 1000.0,
                    render_avg * 1000.0
                );

                second_frames = 0;
                second = Instant::now();
                write_times.clear();
                render_times.clear()
            }
        }
    }
}
