#![no_std]
//! Representing the data on the actual panel

use cortex_m::asm::nop;
use embedded_graphics_core::draw_target::DrawTarget;
use embedded_graphics_core::geometry::{Dimensions, Point, Size};
use embedded_graphics_core::pixelcolor::{Rgb888, RgbColor};
use embedded_graphics_core::primitives::Rectangle;
use embedded_graphics_core::Pixel;
use embedded_hal::digital::v2::OutputPin;
use mbi5153::{ConfigSender, Mbi5153Config, Mbi5153Config2};

/// The number of address lines on the panel.
pub const ADDRESSES: usize = 5;
/// The number of output lines on the panel.
pub const OUTPUTS: usize = 3 * 4;
/// The number of scan lines in one "chunk" on the panel.
pub const SCAN_LINES: usize = 20;
/// The number of "chunks" SCAN_LINES high on the panel.
pub const CHUNK_COUNT: usize = 4;
/// The number of LED output channels each MBI5153 IC can drive.
pub const CHANNELS_PER_LINE: usize = 16;
/// The number of bits for each pixel.
pub const BITS_PER_PIXEL: usize = 16;
/// The number of MBI5153 ICs chained together.
pub const IC_COUNT: usize = 5;
/// The amount of data that gets sent to one chain of `IC_COUNT` ICs.
///
/// There's one chain for each colour, though, and also one set of 3 chains for 1/4 of
/// the display.
pub const DATA_COUNT: usize = SCAN_LINES * CHANNELS_PER_LINE * IC_COUNT;
/// Data for one colour of one "chunk" of the panel (one of the 4 lines that is SCAN_LINES high).
pub type GrayscaleChunkData = [u16; DATA_COUNT];

#[derive(Copy, Clone)]
pub struct ChunkData {
    pub red: GrayscaleChunkData,
    pub green: GrayscaleChunkData,
    pub blue: GrayscaleChunkData,
}

#[derive(Copy, Clone)]
pub struct PanelData(pub [ChunkData; CHUNK_COUNT]);

impl Default for PanelData {
    fn default() -> Self {
        Self(
            [ChunkData {
                red: [0; DATA_COUNT],
                green: [0; DATA_COUNT],
                blue: [0; DATA_COUNT],
            }; CHUNK_COUNT],
        )
    }
}

impl PanelData {
    pub fn pixel_mut(&mut self, x: usize, y: usize) -> (&mut u16, &mut u16, &mut u16) {
        // The data is laid out like:
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 0 -------------------------------------->
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 1 -------------------------------------->
        // ...
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 31 ------------------------------------->

        // Which chunk we are in.
        let chunk_idx = y / SCAN_LINES;
        let chunk = &mut self.0[chunk_idx];
        // Which scan line inside the chunk we are in.
        let scan_line_idx = y - (SCAN_LINES * chunk_idx);
        // Which IC inside the chunk we are in.
        let ic_idx = x / CHANNELS_PER_LINE;
        // Which channel inside the chunk we are in.
        let channel = x - (ic_idx * CHANNELS_PER_LINE);
        // The index into the chunk data based on the above.
        let data_idx = (scan_line_idx * CHANNELS_PER_LINE * IC_COUNT) // gets us into the right scan line
            + (channel * IC_COUNT) // gets us into the right channel
            + ic_idx;

        (
            &mut chunk.red[data_idx],
            &mut chunk.green[data_idx],
            &mut chunk.blue[data_idx],
        )
    }
}

impl Dimensions for PanelData {
    fn bounding_box(&self) -> Rectangle {
        Rectangle::new(
            Point::new(0, 0),
            Size::new(
                (IC_COUNT * CHANNELS_PER_LINE) as _,
                (SCAN_LINES * CHUNK_COUNT) as _,
            ),
        )
    }
}

impl DrawTarget for PanelData {
    type Color = Rgb888;
    type Error = ();

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        for pixel in pixels {
            let (r, g, b) = self.pixel_mut(pixel.0.x as _, pixel.0.y as _);

            #[allow(clippy::unusual_byte_groupings)]
            const EIGHT_TO_14: u16 = 0b1111_1111_1111_11 / 0b1111_1111;

            *r = EIGHT_TO_14 * pixel.1.r() as u16;
            *g = EIGHT_TO_14 * pixel.1.g() as u16;
            *b = EIGHT_TO_14 * pixel.1.b() as u16;
        }
        Ok(())
    }
}

pub const PIO_DATA_LEN: usize = BITS_PER_PIXEL // one pixel
    * IC_COUNT // one channel
    * CHANNELS_PER_LINE // one scanline
    * SCAN_LINES // one frame
    / 2;

pub struct PanelDataPio<'a>(pub &'a mut [u32; PIO_DATA_LEN]);

impl<'a> Dimensions for PanelDataPio<'a> {
    fn bounding_box(&self) -> Rectangle {
        Rectangle::new(
            Point::new(0, 0),
            Size::new(
                (IC_COUNT * CHANNELS_PER_LINE) as _,
                (SCAN_LINES * CHUNK_COUNT) as _,
            ),
        )
    }
}

impl<'a> PanelDataPio<'a> {
    pub fn draw_pixel_8(&mut self, x: usize, y: usize, r: u8, g: u8, b: u8) {
        let r = SRGB_TO_LINEAR_14[r as usize];
        let g = SRGB_TO_LINEAR_14[g as usize];
        let b = SRGB_TO_LINEAR_14[b as usize];

        self.draw_pixel(x, y, r, g, b);
    }
    pub fn draw_pixel(&mut self, x: usize, y: usize, r: u16, g: u16, b: u16) {
        // The data is laid out like:

        // 16 15 14 13 12 11 10  9  8  7  6  5 4 3 2 1
        // B3 R4 G4 B4 R3 G3 B2 G2 R2 B1 G1 R1 0 0 0 0 B3 R4 G4 B4 R3 G3 B2 G2 R2 B1 G1 R1 0 0 0 0
        // <----------------  u16  ------------------> <----------------  u16  ------------------>
        // <-----------------------------------------u32----------------------------------------->
        //
        // 1 channel is 16 u16s (so 8 u32s)
        //
        // and then,
        //
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 0 -------------------------------------->
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 1 -------------------------------------->
        // ...
        // [ IC3, ch15 ] [ IC2, ch15 ] [ IC1, ch15 ] ... [ IC3, ch0  ] [ IC2, ch0  ] [ IC1, ch0  ]
        // <---------------------------------- scan line 31 ------------------------------------->

        const fn bit_position_for(chunk: usize, colour: char) -> usize {
            let gpio = match (colour, chunk + 1) {
                ('R', 1) => 10,
                ('G', 1) => 11,
                ('B', 1) => 12,

                ('R', 2) => 13,
                ('G', 2) => 14,
                ('B', 2) => 15,

                ('R', 3) => 16,
                ('G', 3) => 17,
                ('B', 3) => 18,

                ('R', 4) => 19,
                ('G', 4) => 20,
                ('B', 4) => 21,
                _ => unreachable!(),
            };
            let pin_base = 10;
            gpio - pin_base
        }

        // Which chunk (1/2/3/4) we are in.
        let chunk_idx = y / SCAN_LINES;
        // Which scan line (inside the chunk) we are in.
        let scan_line_idx = (SCAN_LINES - 1) - (y - (SCAN_LINES * chunk_idx));
        // Which IC inside the chunk we are in.
        let ic_idx = x / CHANNELS_PER_LINE;
        // Which channel inside the chunk we are in.
        let channel_idx = x - (ic_idx * CHANNELS_PER_LINE);

        // Which channel we are in overall.
        let channel = (scan_line_idx * CHANNELS_PER_LINE * IC_COUNT) // gets us into the right scan line
            + (channel_idx * IC_COUNT) // gets us into the right channel
            + ic_idx;
        let data = &mut self.0[(channel * 8)..(((channel + 1) * 8) - 1)];

        let rpos = bit_position_for(chunk_idx, 'R');
        let gpos = bit_position_for(chunk_idx, 'G');
        let bpos = bit_position_for(chunk_idx, 'B');

        for (crumb, data) in data.iter_mut().enumerate() {
            let i1 = 14 - (crumb * 2);
            let i2 = 14 - ((crumb * 2) + 1);
            // Get the ith MSB of r.
            let r1 = (r >> i1) & 1;
            let r2 = (r >> i2) & 1;
            // Get the ith MSB of g.
            let g1 = (g >> i1) & 1;
            let g2 = (g >> i2) & 1;
            // Get the ith MSB of b.
            let b1 = (b >> i1) & 1;
            let b2 = (b >> i2) & 1;

            let v1 = ((r1 as u32) << rpos) | ((g1 as u32) << gpos) | ((b1 as u32) << bpos);
            let v2 = ((r2 as u32) << rpos) | ((g2 as u32) << gpos) | ((b2 as u32) << bpos);

            *data |= (v1 << 16) | v2;
        }
    }
}

impl<'a> DrawTarget for PanelDataPio<'a> {
    type Color = Rgb888;
    type Error = ();

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        for pixel in pixels {
            let x = pixel.0.x as usize;
            let y = pixel.0.y as usize;
            if x >= 80 || y >= 80 {
                panic!("bad pixel ({}, {})", x, y);
            }

            self.draw_pixel_8(x, y, pixel.1.r(), pixel.1.g(), pixel.1.b());
        }
        Ok(())
    }
}

pub fn configure_panel<E>(
    config: Mbi5153Config,
    mut dclk: impl OutputPin<Error = E>,
    mut latch: impl OutputPin<Error = E>,
    mut sdi: impl OutputPin<Error = E>,
) -> Result<(), E> {
    latch.set_low()?;
    dclk.set_low()?;
    sdi.set_low()?;
    nop();

    // PRE-ACTIVE
    latch.set_high()?;
    nop();
    for _ in 0..14 {
        dclk.set_low()?;
        nop();
        dclk.set_high()?;
        nop();
    }
    dclk.set_low()?;
    latch.set_low()?;

    let sender = ConfigSender::new(config, IC_COUNT as _);
    for outputs in sender {
        sdi.set_state(outputs.sdis[0].into())?;
        latch.set_state(outputs.le.into())?;
        dclk.set_state(outputs.dclk.into())?;
    }

    dclk.set_low()?;
    latch.set_low()?;
    sdi.set_low()?;

    Ok(())
}

#[allow(clippy::too_many_arguments)]
pub fn configure_panel_2<E>(
    mut dclk: impl OutputPin<Error = E>,
    mut latch: impl OutputPin<Error = E>,
    config_r: Mbi5153Config2,
    mut sdi_r: impl OutputPin<Error = E>,
    config_g: Mbi5153Config2,
    mut sdi_g: impl OutputPin<Error = E>,
    config_b: Mbi5153Config2,
    mut sdi_b: impl OutputPin<Error = E>,
) -> Result<(), E> {
    latch.set_low()?;
    dclk.set_low()?;
    sdi_r.set_low()?;
    sdi_g.set_low()?;
    sdi_b.set_low()?;
    nop();

    // PRE-ACTIVE
    latch.set_high()?;
    nop();
    for _ in 0..14 {
        dclk.set_low()?;
        nop();
        dclk.set_high()?;
        nop();
    }
    dclk.set_low()?;
    latch.set_low()?;

    // FIXME(eta): this is really inefficient!
    let sender_r = ConfigSender::new_2(config_r, IC_COUNT as _);
    let sender_g = ConfigSender::new_2(config_g, IC_COUNT as _);
    let sender_b = ConfigSender::new_2(config_b, IC_COUNT as _);

    for ((outputs_r, outputs_g), outputs_b) in sender_r.into_iter().zip(sender_g).zip(sender_b) {
        sdi_r.set_state(outputs_r.sdis[0].into())?;
        sdi_g.set_state(outputs_g.sdis[0].into())?;
        sdi_b.set_state(outputs_b.sdis[0].into())?;
        latch.set_state(outputs_r.le.into())?;
        dclk.set_state(outputs_r.dclk.into())?;
    }

    dclk.set_low()?;
    latch.set_low()?;

    sdi_r.set_low()?;
    sdi_g.set_low()?;
    sdi_b.set_low()?;

    Ok(())
}

// Generated via:
// fn main() {
//     let data = (0..256).map(|x| {
//         ((x as f64) / 255.0).powf(2.2) * 16384.0
//     }).map(|x| x as u16).map(|x| x.to_string()).collect::<Vec<_>>();
//     println!("pub const SRGB_TO_LINEAR_14: [u16; 256] = [");
//     for chunk in data.chunks(16) {
//         println!("  {},", chunk.join(", "));
//     }
//     println!("];");
// }
#[link_section = ".data"]
pub static SRGB_TO_LINEAR_14: [u16; 256] = [
    0, 0, 0, 0, 1, 2, 4, 6, 8, 10, 13, 16, 19, 23, 27, 32, 37, 42, 48, 54, 60, 67, 74, 82, 90, 98,
    107, 117, 126, 137, 147, 158, 170, 182, 194, 207, 220, 234, 248, 263, 278, 293, 309, 326, 343,
    360, 378, 396, 415, 434, 454, 474, 495, 516, 538, 560, 583, 606, 630, 654, 679, 704, 729, 756,
    782, 809, 837, 865, 894, 923, 953, 983, 1014, 1045, 1077, 1109, 1142, 1175, 1209, 1243, 1278,
    1314, 1350, 1386, 1423, 1461, 1499, 1538, 1577, 1616, 1657, 1697, 1739, 1781, 1823, 1866, 1909,
    1954, 1998, 2043, 2089, 2135, 2182, 2229, 2277, 2326, 2375, 2424, 2474, 2525, 2576, 2628, 2681,
    2734, 2787, 2841, 2896, 2951, 3007, 3063, 3120, 3178, 3236, 3294, 3353, 3413, 3474, 3535, 3596,
    3658, 3721, 3784, 3848, 3912, 3977, 4043, 4109, 4176, 4243, 4311, 4380, 4449, 4519, 4589, 4660,
    4731, 4804, 4876, 4950, 5023, 5098, 5173, 5249, 5325, 5402, 5479, 5557, 5636, 5715, 5795, 5876,
    5957, 6038, 6121, 6204, 6287, 6371, 6456, 6542, 6627, 6714, 6801, 6889, 6978, 7067, 7156, 7247,
    7337, 7429, 7521, 7614, 7707, 7801, 7896, 7991, 8087, 8183, 8281, 8378, 8477, 8576, 8675, 8775,
    8876, 8978, 9080, 9183, 9286, 9390, 9495, 9600, 9706, 9813, 9920, 10028, 10136, 10245, 10355,
    10465, 10576, 10688, 10800, 10913, 11027, 11141, 11256, 11371, 11487, 11604, 11722, 11840,
    11958, 12078, 12198, 12319, 12440, 12562, 12684, 12808, 12932, 13056, 13181, 13307, 13434,
    13561, 13689, 13817, 13946, 14076, 14207, 14338, 14470, 14602, 14735, 14869, 15003, 15138,
    15274, 15410, 15547, 15685, 15823, 15962, 16102, 16242, 16384,
];
