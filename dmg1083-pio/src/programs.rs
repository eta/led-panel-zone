//! The PIO assembly programs used to drive the display.
//!
//! Most users should use the functions in the rest of the crate, instead of using these programs
//! directly.

use pio::{
    JmpCondition, MovDestination, MovOperation, MovSource, OutDestination, Program, SetDestination,
    SideSet, WaitSource,
};

pub const IRQ_GCLK_REQUEST: u8 = 1;
pub const IRQ_GCLK_DONE: u8 = 2;
pub const IRQ_ADDRESS_DONE: u8 = 3;
pub const IRQ_VSYNC_IN_PROGRESS: u8 = 4;

fn invert(bit: u8) -> u8 {
    if bit == 0 {
        1
    } else {
        0
    }
}

fn identity(bit: u8) -> u8 {
    bit
}

fn invert_function(should_invert: bool) -> fn(u8) -> u8 {
    if should_invert {
        invert
    } else {
        identity
    }
}

/// Fills the X and Y scratch registers with one word from the TX FIFO.
///
/// This program uses 3 instructions, but should ideally just be run with forced execution.
pub fn fill_x_y_program(side_set: SideSet) -> Program<32> {
    let mut asm = pio::Assembler::new_with_side_set(side_set);

    // If we have to include a side set for the program to be valid, do so.
    if side_set.optional() {
        // Pull in the word from the TX FIFO
        asm.pull(false, true);
        // Copy the number of bits per latch from the OSR into Y and X.
        asm.mov(MovDestination::Y, MovOperation::None, MovSource::OSR);
        asm.mov(MovDestination::X, MovOperation::None, MovSource::Y);
    } else {
        asm.pull_with_side_set(false, true, 0);
        asm.mov_with_side_set(MovDestination::Y, MovOperation::None, MovSource::OSR, 0);
        asm.mov_with_side_set(MovDestination::X, MovOperation::None, MovSource::Y, 0);
    }
    asm.assemble_program()
}

/// Sends parallel frame data from the TX FIFO to at most 16 output pins, toggling a latch pin at a
/// configurable interval, and toggling a clock pin for every bit of data.
///
/// `should_invert` flips the polarity of the latch and clock pins, for use behind NOT gates. It
/// does not flip the polarity of the frame data.
///
/// # Preconditions
///
/// - The X and Y scratch registers must be filled with the number of bits per latch minus one,
///   using `fill_x_y_program` with optional sideset.
/// - The pin directions for all outputs must be set to `Output`.
/// - The outputs must be configured as sticky.
/// - The FIFOs should be configured as transmit only.
///
/// # Inputs
///
/// Each 32-bit TX FIFO word contains 2 sets of output data (16 bits per set). Each clock cycle
/// will consume 16 bits and shift them out in parallel to the `OUT` pin mapping.
///
/// The state machine will refuse to make progress if `IRQ_VSYNC_IN_PROGRESS` is asserted.
///
/// # Outputs
///
/// - `OUT` pins (0-15): output data, shifted out from the FIFO
/// - `SET` pin: the latch
/// - Side-set pin: the data clock
///
/// # Timing and instructions
///
/// This program holds the data clock LOW for 5 cycles, and HIGH for 5 cycles (disregarding stalls
/// due to vsyncs / TX FIFO being empty).
///
/// This program uses 8 instructions.
pub fn frame_sending_program(should_invert: bool) -> Program<32> {
    let mut asm = pio::Assembler::new_with_side_set(SideSet::new(true, 1, false));
    let mut wrap_target = asm.label();
    let mut wrap_source = asm.label();
    let mut else_branch = asm.label();
    let mut post_if = asm.label();

    let inv = invert_function(should_invert);

    // loop:
    asm.bind(&mut wrap_target);
    // [0]   if X == 0:     [set data clock LOW]
    asm.jmp_with_side_set(JmpCondition::XDecNonZero, &mut else_branch, inv(0));

    // Reset the bits-per-latch counter back to its original value, and toggle the latch for this
    // next bit.
    //
    // [1]   -> set X to Y
    asm.mov(MovDestination::X, MovOperation::None, MovSource::Y);
    // [2]   -> set the latch high
    asm.set(SetDestination::PINS, inv(1));
    // [3]   -> jump to after else branch
    asm.jmp(JmpCondition::Always, &mut post_if);

    // [ ]   else:
    asm.bind(&mut else_branch);

    // [1-3] -> set the latch low, and delay to equal the other branch's cycle count
    //          Note also that X is decremented by one due to the JMP instruction's `XDecNonZero`.
    asm.set_with_delay(SetDestination::PINS, inv(0), 2);

    // [ ]   end if
    asm.bind(&mut post_if);

    // [4]   shift 16 bits of OSR data out to the pins
    asm.out(OutDestination::PINS, 16);
    // [5]   pull in the next word if we need to        [set data clock HIGH]
    //       (strategically stall with the data clock set HIGH, so another state machine doesn't
    //        accidentally clock in data when taking control)
    asm.pull_with_side_set(true, true, inv(1));
    // [6-10]make sure there's no VSYNC in progress (stall if there is),
    //       and delay to equal the number of cycles that the clock was held LOW
    asm.wait_with_delay(0, WaitSource::IRQ, IRQ_VSYNC_IN_PROGRESS, false, 4);

    // end loop body
    asm.bind(&mut wrap_source);

    asm.assemble_with_wrap(wrap_source, wrap_target)
}

/// Count down scanlines in a loop, shifting out the current scanline in binary to a set of
/// address lines, and requesting a pulse of GCLK cycles for each scan line.
///
/// Also, check for a waiting vsync state machine once the counter reaches zero, and wait for
/// this to complete if there is one.
///
/// `should_invert` flips the polarity of the values written to the address lines.
///
/// # Preconditions
///
/// - The X and Y scratch registers must be filled with the number of scanlines minus one,
///   using `fill_x_y_program` with optional sideset.
/// - The pin directions for all outputs must be set to `Output`.
/// - The outputs must be configured as sticky.
/// - The `gclk_program` must be running on the same PIO block, in another state machine.
/// - The `vsync_program` should probably be running on the same PIO block, in a higher state
///   machine than this one.
/// - The `vsync_program` must not be set to run at a faster clock rate than this program (else
///   they will not synchronize properly).
///
/// # Outputs
///
/// - `OUT` pins (0-15): up to 16 address lines, to which the current scanline is written in
///    binary form (endianness dependent on `ShiftDirection` I assume)
///
/// # Timing and instructions
///
/// FIXME(eta): figure out how the dead time works exactly and make sure it's correct
///
/// This program uses 8 instructions.
pub fn scanout_program(should_invert: bool) -> Program<32> {
    let mut asm = pio::Assembler::new();

    let mut wrap_target = asm.label();
    let mut wrap_source = asm.label();
    let mut else_branch = asm.label();

    // loop:
    asm.bind(&mut wrap_target);

    // wait for the GCLK program to finish
    asm.wait(1, WaitSource::IRQ, IRQ_GCLK_DONE, false);

    // if X == 0:
    asm.jmp(JmpCondition::XDecNonZero, &mut else_branch);

    // -> set X to Y (with 31-cycle delay for dead time purposes)
    asm.mov_with_delay(MovDestination::X, MovOperation::None, MovSource::Y, 31);
    // -> assert IRQ_ADDRESS_DONE to notify a waiting vsync program that it's safe to start vsync
    asm.irq_with_delay(false, false, IRQ_ADDRESS_DONE, false, 31);
    // -> wait for IRQ_VSYNC_IN_PROGRESS to be zero
    //    If there's a vsync program running, we will wait for it (since it asserts that IRQ).
    //    If there isn't one, we will not wait at all (since it will already be zero).
    asm.wait(0, WaitSource::IRQ, IRQ_VSYNC_IN_PROGRESS, false);
    // -> clear IRQ_ADDRESS_DONE in case there wasn't a vsync program waiting
    //    (if we don't do this, a vsync program might start up and
    asm.irq_with_delay(true, false, IRQ_ADDRESS_DONE, false, 31);

    // end if
    asm.bind(&mut else_branch);

    // move the counter onto the address lines
    asm.mov(
        MovDestination::PINS,
        if should_invert {
            MovOperation::Invert
        } else {
            MovOperation::None
        },
        MovSource::X,
    );
    // request a new GCLK burst
    asm.irq(false, true, IRQ_GCLK_REQUEST, false);

    // end loop body
    asm.bind(&mut wrap_source);

    asm.assemble_with_wrap(wrap_source, wrap_target)
} // -> 12 instrs (21 total)

/// Send a configurable number of `GCLK` pulses in a burst when requested by the `scanout_program`.
///
/// `should_invert` flips the polarity of the clock.
///
/// # Preconditions
///
/// - The X and Y scratch registers must be filled with the number of scanlines minus one,
///   using `fill_x_y_program` with **mandatory** sideset using one pin.
/// - The pin directions for all outputs must be set to `Output`.
/// - The `scanout_program` should be running in the same PIO block.
///
/// # Outputs
///
/// - Side-set pin: the grayscale clock (`GCLK`) line
///
/// # Timing and instructions
///
/// When doing a burst, the program holds GCLK low for one cycle, and high for one cycle.
/// When the burst comes to an end, the program delays for 16 cycles and then stalls until another
/// burst is requested.
///
/// This program uses 5 instructions.
pub fn gclk_program(should_invert: bool) -> Program<32> {
    let mut asm = pio::Assembler::new_with_side_set(SideSet::new(false, 1, false));

    let mut wrap_target = asm.label();
    let mut wrap_source = asm.label();
    let mut else_branch = asm.label();

    let inv = invert_function(should_invert);

    // loop:
    asm.bind(&mut wrap_target);

    // [0] if X == 0:   [GCLK = low]
    asm.jmp_with_side_set(JmpCondition::XDecNonZero, &mut else_branch, inv(0));

    // [1] -> set X := Y, with 15-cycle delay
    asm.mov_with_delay_and_side_set(
        MovDestination::X,
        MovOperation::None,
        MovSource::Y,
        15,
        inv(0),
    );
    // [2] -> tell the scanouter this burst is done
    asm.irq_with_side_set(false, true, IRQ_GCLK_DONE, false, inv(0));
    // [3] -> wait for a new burst request
    asm.wait_with_side_set(1, WaitSource::IRQ, IRQ_GCLK_REQUEST, false, inv(0));

    // [ ] end if
    asm.bind(&mut else_branch);

    // [1] drive GCLK high
    asm.nop_with_side_set(inv(1));

    // end loop body
    asm.bind(&mut wrap_source);

    asm.assemble_with_wrap(wrap_source, wrap_target)
}

/// Wait for vsync requests from the CPU (using the TX FIFO) and send out a vsync command when
/// one is received, coordinating with the `scanout_program` to send this at the end of a frame.
///
/// `should_invert` flips the polarity of the data clock and latch.
///
/// # Preconditions
///
/// - The pin directions for all outputs must be set to `Output`.
/// - The `scanout_program` should be running in the same PIO block.
///
/// # Inputs
///
/// Sending anything over the TX FIFO will cause a vsync to be scheduled.
///
/// # Outputs
///
/// - Side-set pin: the grayscale clock (`GCLK`) line
/// - `SET` pin: the data latch
///
/// # Timing and instructions
///
/// FIXME(eta): add timing note
///
/// This program uses 9 instructions.
// FIXME(eta): It could probably use less, and be generalised to send arbitrary commands, too!
pub fn vsync_program(should_invert: bool) -> Program<32> {
    let mut asm = pio::Assembler::new_with_side_set(SideSet::new(true, 1, false));

    let mut wrap_target = asm.label();
    let mut wrap_source = asm.label();

    let inv = invert_function(should_invert);

    // loop:
    asm.bind(&mut wrap_target);

    // Wait for a request for vsync from the CPU.
    asm.pull(false, true);
    // Wait for the scanout program to complete scanning out the current frame.
    asm.wait(1, WaitSource::IRQ, IRQ_ADDRESS_DONE, false);
    // Indicate we are going to do a vsync by asserting IRQ_VSYNC_IN_PROGRESS,
    // which stops the scanout program from starting a new frame while we do a vsync.
    asm.irq(false, false, IRQ_VSYNC_IN_PROGRESS, false);

    // Do the actual vsync sequence:
    //
    // Assert latch
    asm.set_with_side_set(SetDestination::PINS, inv(1), inv(0));
    // Clock DCLK twice with latch held high
    asm.set_with_side_set(SetDestination::PINS, inv(1), inv(1));
    asm.set_with_side_set(SetDestination::PINS, inv(1), inv(0));
    asm.set_with_side_set(SetDestination::PINS, inv(1), inv(1));
    // Deassert both pins
    asm.set_with_side_set(SetDestination::PINS, inv(0), inv(0));

    // Deassert IRQ_VSYNC_IN_PROGRESS to unlock the scanout program.
    asm.irq(true, false, IRQ_VSYNC_IN_PROGRESS, false);

    // end loop body
    asm.bind(&mut wrap_source);

    asm.assemble_with_wrap(wrap_source, wrap_target)
}
