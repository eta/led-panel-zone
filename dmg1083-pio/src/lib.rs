#![no_std]

use crate::programs::IRQ_ADDRESS_DONE;
use dmg1083::{ADDRESSES, BITS_PER_PIXEL, IC_COUNT, OUTPUTS, SCAN_LINES};
use pio::{Instruction, SideSet};
use rp2040_hal::dma::{single_buffer, ReadTarget, SingleChannel};
use rp2040_hal::gpio::{
    DynFunction, DynPinId, DynPullType, FunctionPio0, FunctionPio1, Pin, PullType,
};
use rp2040_hal::pac::{PIO0, PIO1, RESETS};
use rp2040_hal::pio::{
    Buffers, PIOBuilder, PIOExt, PinDir, Running, ShiftDirection, StateMachine, Stopped, Tx,
    ValidStateMachine, PIO, SM0, SM1, SM2, SM3,
};

pub mod programs;

type DynPin = Pin<DynPinId, DynFunction, DynPullType>;
pub type Pio0Pin = Pin<DynPinId, FunctionPio0, DynPullType>;
pub type Pio1Pin = Pin<DynPinId, FunctionPio1, DynPullType>;

pub struct Dmg1083<P: PIOExt> {
    _outputs: [DynPin; OUTPUTS],
    _addrs: [DynPin; ADDRESSES],
    _dclk: DynPin,
    _gclk: DynPin,
    _latch: DynPin,
    _pio: PIO<P>,
    _sm0: StateMachine<(P, SM0), Running>,
    _sm1: StateMachine<(P, SM1), Running>,
    _sm2: StateMachine<(P, SM2), Running>,
    _sm3: StateMachine<(P, SM3), Running>,
    vsync_tx: Tx<(P, SM3)>,
    frame_tx: Option<Tx<(P, SM0)>>,
}

/// Checks that the `pins` are sequential (in lowest-highest order), and
/// returns `Some(base)` with the lowest pin number if so.
fn check_sequential(pins: &[DynPin]) -> Option<u8> {
    let base = pins[0].id().num;
    let mut cur = base;

    for output in pins[1..].iter() {
        if output.id().num != cur + 1 {
            return None;
        }
        cur = output.id().num;
    }
    Some(base)
}

/// Run the `fill_x_y_program` on the provided state machine and TX FIFO, sending the provided
/// `value`. `side_set` is the side set configuration of the state machine's program.
fn fill_x_y<S: ValidStateMachine>(
    sm: &mut StateMachine<S, Stopped>,
    tx: &mut Tx<S>,
    value: u32,
    side_set: SideSet,
) {
    sm.drain_tx_fifo();
    assert!(tx.write(value));

    let preamble = programs::fill_x_y_program(side_set);
    for instr in preamble.code.iter() {
        sm.exec_instruction(Instruction::decode(*instr, preamble.side_set).unwrap());
    }
}

impl Dmg1083<PIO0> {
    pub fn new_pio0<U: PullType>(
        pio: PIO0,
        resets: &mut RESETS,
        should_invert: bool,
        gclk_multiplier: bool,
        dclk: Pin<DynPinId, FunctionPio0, U>,
        gclk: Pin<DynPinId, FunctionPio0, U>,
        latch: Pin<DynPinId, FunctionPio0, U>,
        outputs: [Pin<DynPinId, FunctionPio0, U>; OUTPUTS],
        addresses: [Pin<DynPinId, FunctionPio0, U>; ADDRESSES],
    ) -> Self {
        Self::new_generic(
            pio,
            resets,
            should_invert,
            gclk_multiplier,
            dclk.into_pull_type().try_into_function().ok().unwrap(),
            gclk.into_pull_type().try_into_function().ok().unwrap(),
            latch.into_pull_type().try_into_function().ok().unwrap(),
            outputs.map(|x| x.into_pull_type().try_into_function().ok().unwrap()),
            addresses.map(|x| x.into_pull_type().try_into_function().ok().unwrap()),
        )
    }
}

impl Dmg1083<PIO1> {
    pub fn new_pio1<U: PullType>(
        pio: PIO1,
        resets: &mut RESETS,
        should_invert: bool,
        gclk_multiplier: bool,
        dclk: Pin<DynPinId, FunctionPio1, U>,
        gclk: Pin<DynPinId, FunctionPio1, U>,
        latch: Pin<DynPinId, FunctionPio1, U>,
        outputs: [Pin<DynPinId, FunctionPio1, U>; OUTPUTS],
        addresses: [Pin<DynPinId, FunctionPio1, U>; ADDRESSES],
    ) -> Self {
        Self::new_generic(
            pio,
            resets,
            should_invert,
            gclk_multiplier,
            dclk.into_pull_type().try_into_function().ok().unwrap(),
            gclk.into_pull_type().try_into_function().ok().unwrap(),
            latch.into_pull_type().try_into_function().ok().unwrap(),
            outputs.map(|x| x.into_pull_type().try_into_function().ok().unwrap()),
            addresses.map(|x| x.into_pull_type().try_into_function().ok().unwrap()),
        )
    }
}

impl<P: PIOExt> Dmg1083<P> {
    pub fn new_generic(
        pio: P,
        resets: &mut RESETS,
        should_invert: bool,
        gclk_multiplier: bool,
        dclk: DynPin,
        gclk: DynPin,
        latch: DynPin,
        outputs: [DynPin; OUTPUTS],
        addresses: [DynPin; ADDRESSES],
    ) -> Self {
        // Verify that the pins that need to be sequential are.
        let outputs_base = check_sequential(&outputs)
            .expect("Outputs provided to Dmg1083::new must be sequential, in low-high order");
        let addresses_base = check_sequential(&addresses)
            .expect("Addresses provided to Dmg1083::new must be sequential, in low-high order");

        // Verify that the pins are configured with the correct PIO function.
        let expected_function = match P::id() {
            0 => DynFunction::Pio0,
            1 => DynFunction::Pio1,
            _ => unreachable!(),
        };
        if outputs
            .iter()
            .chain(addresses.iter())
            .chain([&dclk, &gclk, &latch])
            .any(|x| x.function() != expected_function)
        {
            panic!(
                "Pins provided to Dmg1083::new must be configured with the appropriate Function"
            );
        }

        let (mut pio, sm0, sm1, sm2, sm3) = pio.split(resets);

        // Configure the frame sending program.

        let frame_sender_p = programs::frame_sending_program(should_invert);
        let frame_sender_i = pio.install(&frame_sender_p).unwrap();

        let (mut sm0, _, mut tx0) = PIOBuilder::from_program(frame_sender_i)
            .side_set_pin_base(dclk.id().num)
            .set_pins(latch.id().num, 1)
            .out_pins(outputs_base, OUTPUTS as _)
            .clock_divisor_fixed_point(8, 0)
            .out_sticky(true)
            .out_shift_direction(ShiftDirection::Left)
            .buffers(Buffers::OnlyTx)
            .build(sm0);

        sm0.set_pindirs(outputs.iter().map(|x| (x.id().num, PinDir::Output)).chain([
            (dclk.id().num, PinDir::Output),
            (latch.id().num, PinDir::Output),
        ]));

        // Prime the frame sending program with the number of bits per word (minus one).
        fill_x_y(
            &mut sm0,
            &mut tx0,
            ((IC_COUNT * BITS_PER_PIXEL) - 1) as _,
            frame_sender_p.side_set,
        );

        // Configure the scanout program.

        let scanout_p = programs::scanout_program(should_invert);
        let scanout_i = pio.install(&scanout_p).unwrap();

        let (mut sm1, _, mut tx1) = PIOBuilder::from_program(scanout_i)
            .out_pins(addresses_base, ADDRESSES as _)
            .clock_divisor_fixed_point(40, 0)
            .out_sticky(true)
            .build(sm1);

        sm1.set_pindirs(addresses.iter().map(|x| (x.id().num, PinDir::Output)));

        // Prime the frame sending program with the number of scan lines (minus one).
        fill_x_y(
            &mut sm1,
            &mut tx1,
            (SCAN_LINES - 1) as _,
            scanout_p.side_set,
        );

        // Configure the GCLK program.

        let gclk_p = programs::gclk_program(should_invert);
        let gclk_i = pio.install(&gclk_p).unwrap();

        let (mut sm2, _, mut tx2) = PIOBuilder::from_program(gclk_i)
            .side_set_pin_base(gclk.id().num)
            .clock_divisor_fixed_point(16, 0)
            .build(sm2);

        sm2.set_pindirs([(gclk.id().num, PinDir::Output)]);

        // Write the number of GCLKs per scanline
        fill_x_y(
            &mut sm2,
            &mut tx2,
            if gclk_multiplier { 256 } else { 512 } as _,
            gclk_p.side_set,
        );

        // Configure the vsync program.

        let vsync_p = programs::vsync_program(should_invert);
        let vsync_i = pio.install(&vsync_p).unwrap();

        let (mut sm3, _, tx3) = PIOBuilder::from_program(vsync_i)
            .side_set_pin_base(dclk.id().num)
            .set_pins(latch.id().num, 1)
            .clock_divisor_fixed_point(16, 0)
            .build(sm3);

        sm3.set_pindirs([
            (dclk.id().num, PinDir::Output),
            (latch.id().num, PinDir::Output),
        ]);
        sm3.drain_tx_fifo();

        let sm0 = sm0.start();
        let sm3 = sm3.start();
        let sm1 = sm1.start();
        let sm2 = sm2.start();

        Self {
            _outputs: outputs,
            _addrs: addresses,
            _dclk: dclk,
            _gclk: gclk,
            _latch: latch,
            _pio: pio,
            _sm0: sm0,
            _sm1: sm1,
            _sm2: sm2,
            _sm3: sm3,
            vsync_tx: tx3,
            frame_tx: Some(tx0),
        }
    }

    /// Write one word of a frame.
    pub fn write_frame_word(&mut self, word: u32) {
        // Retry writing until it succeeds.
        let frame_tx = self.frame_tx.as_mut().unwrap();

        while !frame_tx.write(word) {}
    }

    /// Transfer the provided framebuffer to the panel with DMA.
    //
    // FIXME(eta): make the type of `frame` nicer
    pub fn transmit_frame<D: SingleChannel>(
        &mut self,
        frame: impl ReadTarget<ReceivedWord = u32>,
        dma: D,
    ) -> D {
        let frame_tx = self.frame_tx.take().unwrap();

        let transfer = single_buffer::Config::new(dma, frame, frame_tx).start();
        let (dma, _, frame_tx) = transfer.wait();

        self.frame_tx = Some(frame_tx);
        dma
    }

    // FIXME(eta): This is a hack!
    pub fn frame_tx_mut(&mut self) -> &mut Option<Tx<(P, SM0)>> {
        &mut self.frame_tx
    }

    /// Request a vsync.
    pub fn vsync(&mut self) {
        let frame_tx = self.frame_tx.as_mut().unwrap();

        // wait for the FIFO to fully drain
        while !frame_tx.is_empty() {}
        // introduce one frame of delay to prevent tearing
        while self._pio.get_irq_raw() & IRQ_ADDRESS_DONE == 0 {}
        while self._pio.get_irq_raw() & IRQ_ADDRESS_DONE > 0 {}

        self.vsync_tx.write(0);
        // FIXME(eta): whaaat????
        for _ in 0..50 {
            // introduce one frame of delay to prevent tearing
            while self._pio.get_irq_raw() & IRQ_ADDRESS_DONE == 0 {}
            while self._pio.get_irq_raw() & IRQ_ADDRESS_DONE > 0 {}
        }
    }
}
